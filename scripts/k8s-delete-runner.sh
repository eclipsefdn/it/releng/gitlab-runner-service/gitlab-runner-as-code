#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Delete runner 

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

: "${GRAC_DRY:=""}"
[[ -n "${GRAC_DRY}" ]] && [[ "${GRAC_DRY}" == "true" ]] && \
  log info "Dry Mode: ON, delete is deactivated!" && exit 0

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"
 
log info  "############################ START DELETE RUNNER ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instance="${1:-}"
deployment="${2:-}"

start=$(date +"%s")

CONFIG_JSON="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/config.json"


if [[ ! -f "${CONFIG_JSON}" ]]; then
  log warn "Config file not found"
  exit 
fi

if [ -z "${instance}" ]; then
  log error "You must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log error "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 1
fi

log warn "Warning: you are about to delete gitlab runner from gitlab ${instance} ${deployment}"

sleep 5

log info "Deleting gitlab runner ${instance} ${deployment}..."

deleteGitLabRunner() {
  local namespace=$1
  local tagList=$2

  if [[ -z "${GRAC_DRY}" ]] || [[ "${GRAC_DRY}" == "false" ]]; then
    log info "Deleting gitlab runner from namespace ${namespace} with taglist: ${tagList}..."
    "${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "delete_runner" "${namespace}" "" "${tagList}"
  else
    log info "[DRY MODE] Deleting gitlab runner from namespace ${namespace} with taglist: ${tagList}..."
  fi
}


allDeploymentTypes="$(jq -r '.deploymentctx | to_entries' "${CONFIG_JSON}" | jq -r '.[].key')"
for deploymentctx in ${allDeploymentTypes} 
do
  deploymentCtxNamespace="$(jq  -r --arg deploymentctx "${deploymentctx}" '.deploymentctx | .[$deploymentctx] | .namespace' "${CONFIG_JSON}")"
  tagList="$(jq -r --arg deploymentType "${deploymentctx}" '.deploymentctx |  .[$deploymentType] | .runner.env.runner.gracDefaultTagList' "${CONFIG_JSON}")"
      
  if [[ -n "${deployment}" ]]; then
    if [[ "${deployment}" == "${deploymentctx}" ]]; then
        deleteGitLabRunner "${deploymentCtxNamespace}" "${tagList}"
      break
    fi
  else
    deleteGitLabRunner "${deploymentCtxNamespace}" "${tagList}"
  fi 
done


end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END START DELETE RUNNER in ${runtime}s ############################"