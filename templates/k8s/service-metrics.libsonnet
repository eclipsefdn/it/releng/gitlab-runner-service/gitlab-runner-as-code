// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
{
  gen(config, configctx, ctx): Kube.ServiceMetrics(config.project.shortName, config, ctx) {
    spec: {
      ports: [
        {
          name: 'metrics',
          port: config.deployment.runner.global.metricsPort,
          protocol: 'TCP',
          targetPort: std.substr(ctx,0,11) + '-mcs',
        },
      ],
      selector: {
        [config.project.organizationFullName + '/project.fullname']: config.project.fullName
      },
    },
  },
}
