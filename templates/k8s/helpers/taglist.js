// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

let taglist = function () {};

function toSnakeCase(inputString) {
  return inputString.split('').map((character) => {
      if (character == character.toUpperCase()) {
          return '_' + character.toUpperCase();
      } else {
          return character;
      }
  })
  .join('');
}

taglist.register = function (Handlebars) {
  Handlebars.registerHelper('taglist', function(prefix, options) {
    let envTagList="";
    options.fn(this).split(",").forEach(taglistElement => {
      let taglistElementKV = taglistElement.split(":");
      if(taglistElementKV != "" && taglistElementKV.length == 2){
        envTagList += "            \""+ prefix+toSnakeCase(taglistElementKV[0]).toUpperCase() + "=" + taglistElementKV[1]+"\",\n"
      }
    });
    return envTagList;
  });
};

module.exports = taglist;