// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0
{

  build_cpu_request::1000,
  build_cpu_limit::2000,
  build_mem_request::4*1024,
  build_mem_limit::4*1024,

  build_storage_request::10*1000,
  build_storage_limit::20*1000,
  
  build_concurrent::1,
  build_concurrent_increment_per_pack::1,

} 
  
 
  