<!--
SPDX-FileCopyrightText: 2024 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# Multi-runner

## Default runner
Add multiple runner to a project or group.

By default, only one runner is generate and deployment file are suffixed with `-default`keyword.

* `configmap-default.yaml`
* `deployment-default.json`

## Adding a new runner 

Adding a new runner volume in `instances/my_project/okd-cx/config.jsonnet` 

For that you need to specify the name of the deployment, it will be used as `context`.

```json
  project+: {
    fullName: "XXXXXXXXXX",
  },
```

With this exemple, those files are generated:

* `configmap-default.yaml`
* `deployment-default.json`
* `configmap-kaniko.yaml`
* `deployment-kaniko.json`


Runner deploy will have a specific tag: `ctx:default` and `ctx:kaniko`

## Adding specification to our new runner 

Ex: 
* add a NFS volume to `default`
* specify a specific ENV to `kaniko`

```json
deploymentctx+:{
    default+: {
      runner+:{
        runners+:{
          env+:["CI_RUNNER_PERSISTENT_STORAGE=/var/shared"],
          kubernetes+:{
            volumes+:{        
              pvc: [
                {
                  name: $['config.json'].project.kubeNamespaceName + '-' + Kube.namingPersistentVolumeClaim,
                  mount_path: '/var/shared'
                },
              ],
            },
          },
        },
        env+:{
          // kubernetes+:{
          //   buildContainerSecurityContextReadOnlyRootFilesystem: "false"
          // },
          runner+:{
            outputLimit:'100000'
          }
        }
      },
    },
    kaniko+: {
      runner+:{
        env+:{
          kubernetes+:{
            buildContainerSecurityContextRunAsUser:"0"
          }
        }
      },
    }
  }
  ```