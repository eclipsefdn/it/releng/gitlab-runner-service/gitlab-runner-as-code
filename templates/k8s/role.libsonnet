// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
{
  gen(config): Kube.Role(config.project.kubeNamespaceName, config) {
    rules: [
      {
        apiGroups: [
          '',
        ],
        resources: [
          'events',
        ],
        verbs: [
          'list',
          'get',
          'watch',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'services',
        ],
        verbs: [
          'list',
          'get',
          'watch',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'pods',
        ],
        verbs: [
          'list',
          'get',
          'watch',
          'create',
          'delete',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'pods/exec',
        ],
        verbs: [
          'create',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'pods/log',
        ],
        verbs: [
          'get',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'pods/attach',
        ],
        verbs: [
          'list',
          'get',
          'create',
          'delete',
          'update',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'secrets',
        ],
        verbs: [
          'create',
          'delete',
          'update',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'configmaps',
        ],
        verbs: [
          'list',
          'get',
          'create',
          'delete',
          'update',
        ],
      },
      {
        apiGroups: [
          '',
        ],
        resources: [
          'serviceaccounts',
        ],
        verbs: [
          'list',
          'get',
          'create',
          'delete',
          'update',
        ],
      },
    ],
  },
}
