<!--
SPDX-FileCopyrightText: 2024 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# Formatting jsonnet

Formating is based on jsonnetfmt.

See vscode plugin: https://marketplace.visualstudio.com/items?itemName=xrc-inc.jsonnet-formatter



```shell
jsonnetfmt --indent 2 --max-blank-lines 2 --sort-imports --string-style s --comment-style s
```