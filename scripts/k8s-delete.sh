#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Delete instance

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

: "${GRAC_DRY:=""}"
[[ -n "${GRAC_DRY}" ]] && [[ "${GRAC_DRY}" == "true" ]] && \
  log info "Dry Mode: ON, delete is deactivated!" && exit 0

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"
 
log info  "############################ START K8S_DELETE ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instance="${1:-}"
deployment="${2:-}"

start=$(date +"%s")

CONFIG_JSON="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/config.json"


if [[ ! -f "${CONFIG_JSON}" ]]; then
  log warn "Config file not found"
  exit 
fi


if [ -z "${instance}" ]; then
  log error "You must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log error "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 1
fi

log warn "Warning: you are about to delete ${instance} in cluster ${CURRENT_CLUSTER_CONTEXT}. This may result in data loss."

sleep 5

log info "Deleting ${instance} in cluster ${CURRENT_CLUSTER_CONTEXT}..."

# delete k8s 

#TODO delete namespace.json

if [[ -n "${deployment}" ]]; then      
  oc delete -f ${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/statefulset-${deployment}.json --request-timeout=2m || true
else
  find ${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/ -type f -not -name 'namespace*' -print0  | \
  (xargs -0 -I {} oc delete -f {} --request-timeout=2m || true)
fi 

end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END K8S_DELETE in ${runtime}s ############################"