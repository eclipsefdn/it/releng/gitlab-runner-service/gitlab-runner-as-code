#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Deploy instance

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"
 
# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/gitlab_context.sh"

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

: "${GRAC_DRY:=""}"
[[ -n "${GRAC_DRY}" ]] && [[ "${GRAC_DRY}" == "true" ]] && \
  log info "Dry Mode: ON, deploy is deactivated!" && exit 0

IFS=$'\n\t'


log info  "############################ START K8S_DEPLOY ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instance="${1:-}"
deployment="${2:-}"

start=$(date +"%s")

if [ -z "${instance}" ]; then
  log error  "You must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log error "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 1
fi

CONFIG_JSON="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/config.json"

if [[ ! -f "${CONFIG_JSON}" ]]; then
  log error "No config.json found at '${CONFIG_JSON}', please run first './grac.sh k8s ${instance}'"
  exit 1
fi

create_resource() {
  local resource_name="${1:-}"
  [[ -f "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/${resource_name}" ]] && \
    oc apply -f "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/${resource_name}" --request-timeout=2m
  return 0
}

create_resource "namespace.json"

# apply all configuration.
find "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s" \
  -type f \( -iname "*" ! -iname "namespace*" ! -iname "statefulset*" \) \
  | parallel oc apply -f {} --request-timeout=2m
  # -exec oc apply -f {} --request-timeout=2m \;

# manage secretsmanager CA
secretsmanager_active="$(jq  -r '.project.secretsManager.active' "${CONFIG_JSON}")"
useVaultNamespaceCA="$(jq  -r '.project.secretsManager.useVaultNamespaceCA' "${CONFIG_JSON}")"

if [[ "${secretsmanager_active}" == "true" ]] && [[ "${useVaultNamespaceCA}" == "true" ]]; then
  SECRETESMANAGER_NAMESPACE="foundation-internal-infra-secretsmanager"
  secretsmanager_env="$(jq  -r '.project.secretsManager.env' "${CONFIG_JSON}")"
  
  [[ -n "${secretsmanager_env}" ]] && SECRETESMANAGER_NAMESPACE+="-${secretsmanager_env}"

  target_namespace="$(jq -r '.metadata.name' "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/namespace.json")"
  log info "SecretsManager is active, deploy secretsmanager CA from ${SECRETESMANAGER_NAMESPACE} to namespace: ${target_namespace}"
  kubectl delete secret secretsmanager-ha-tls -n "${target_namespace}" --ignore-not-found
  kubectl get secret secretsmanager-ha-tls -n "${SECRETESMANAGER_NAMESPACE}" -o yaml \
    | sed s/"namespace: ${SECRETESMANAGER_NAMESPACE}"/"namespace: ${target_namespace}"/\
    | kubectl apply -n "${target_namespace}" -f -

fi

deployment_status() {
  local deploymentFile="${1:-}"
  oc get statefulsets -n "$(jq -r '.metadata.name' "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/namespace.json")" "$(jq -r '.metadata.name' "${deploymentFile}")" -o json
}

deploy() {

  local deploymentType="${1:-}"

  local deploymentFile="${instance}"/"${CURRENT_CLUSTER_CONTEXT}"/target/k8s/statefulset-${deploymentType}.json
  
  deploymentCtxNamespace="$(jq  -r --arg deploymentctx "${deploymentType}" '.deploymentctx | .[$deploymentctx] | .namespace' "${CONFIG_JSON}")"
  tagList="$(jq -r --arg deploymentType "${deploymentType}" '.deploymentctx |  .[$deploymentType] | .runner.env.runner.gracDefaultTagList' "${CONFIG_JSON}")"
  runner_list=$("${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "get_runner_list" "${deploymentCtxNamespace}" "" "${tagList}")

  [[ -n "${runner_list}" ]] && [[ "${runner_list}" != "[]" ]] && log info "Runner already deployed: $(echo "${runner_list}" | jq -r '.[] | [.id, .description] | @csv')"

  old_gen=""
  if deployment_status "${deploymentFile}" &> /dev/null; then
    old_gen=$(deployment_status "${deploymentFile}" | jq -r '.metadata.generation')
  fi

  create_resource "statefulset-${deploymentType}.json"

  if [[ -z "${old_gen}" ]]; then
    log info "Deploying a brand new instance in ctx: ${deploymentType}..."
  else
    log info "Restart statefulset, ctx: ${deploymentType}"
    "${SCRIPT_FOLDER}/k8s-restart.sh" "${instance}" "${deploymentType}" || :
  fi

  runner_list=$("${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "get_runner_list" "${deploymentCtxNamespace}" "" "${tagList}")
  log info "Runner deployed: $(echo "${runner_list}" | jq -r '.[] | [.id, .description] | @csv')"

  runnerType="$(jq  -r --arg deploymentctx "${deploymentType}" '.deploymentctx | .[$deploymentctx] | .runnerType' "${CONFIG_JSON}")"
  if [[ ${runnerType} == "groups" ]]; then
    log info "${GITLAB_URL}/groups/${deploymentCtxNamespace}/-/runners/$(echo "${runner_list}" | jq -r '.[].id')"
  else
    log info "${GITLAB_URL}/${deploymentCtxNamespace}/-/runners/$(echo "${runner_list}" | jq -r '.[].id')"
  fi

  if [[ "${secretsmanager_active}" == "true" ]]; then
    secretsmanager_server_url="$(jq  -r '.project.secretsManager.secretsManagerServerUrl' "${CONFIG_JSON}")"
    secretsmanager_auth_role="$(jq  -r '.project.secretsManager.secretsManagerAuthRole' "${CONFIG_JSON}")"
    secretsmanager_auth_path="$(jq  -r '.project.secretsManager.secretsManagerAuthPath' "${CONFIG_JSON}")"
    echo "Update CI variables in namespace ${deploymentCtxNamespace}: server_url: ${secretsmanager_server_url}, auth_role: ${secretsmanager_auth_role}, auth_path: ${secretsmanager_auth_path}"
    [[ -n "${secretsmanager_server_url}" ]] && "${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "update_CI_variables" "${deploymentCtxNamespace}" "${runnerType}" "VAULT_SERVER_URL" "${secretsmanager_server_url}"
    [[ -n "${secretsmanager_auth_role}" ]] && "${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "update_CI_variables" "${deploymentCtxNamespace}" "${runnerType}" "VAULT_AUTH_ROLE" "${secretsmanager_auth_role}"
    [[ -n "${secretsmanager_auth_path}" ]] && "${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "update_CI_variables" "${deploymentCtxNamespace}" "${runnerType}" "VAULT_AUTH_PATH" "${secretsmanager_auth_path}"

  fi
}

allDeploymentTypes="$(jq -r '.deploymentctx | to_entries' "${CONFIG_JSON}" | jq -r '.[].key')"
for deploymentctx in ${allDeploymentTypes} 
do
  if [[ -n "${deployment}" ]]; then
    if [[ "${deployment}" == "${deploymentctx}" ]]; then
        deploy "${deploymentctx}"
      break
    fi
  else
    deploy "${deploymentctx}"
  fi 
done

end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END K8S_DEPLOY in ${runtime}s ############################"