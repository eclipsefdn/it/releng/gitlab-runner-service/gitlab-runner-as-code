// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
{
  gen(config, configctx, ctx): Kube.ServiceSessionServer(config.project.shortName, config, ctx) {
    spec: {
      type: "NodePort",
      ports: [
        {
          name: 'session-server',
          port: config.deployment.runner.runners.kubernetes.sessionServer.externalPort,
          protocol: 'TCP',
          targetPort: std.substr(ctx,0,12) + '-ss',
        },
      ],
      selector: {
        [config.project.organizationFullName + '/project.fullname']: config.project.fullName
      },
    },
  },
}
