// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import './k8s/kube.libsonnet';
local Const = std.extVar('packs');
{
  grac: {
    version: std.extVar('gracVersion'),
    generationDate: std.extVar('currentDate'),
  },
  project: {
    organization: 'org.eclipse.cbi',
    organizationFullName: if self.prefix == '' then self.organization else self.organization + '.' + self.prefix,
    fullName: '',#error 'Must set "project.fullName"',
    shortName: std.split(self.fullName, '.')[std.length(std.split(self.fullName, '.')) - 1],
    KubeFullName: std.strReplace(self.shortName, '.', '-'),
    kubeNamespaceName: if self.prefix == '' then self.KubeFullName else self.prefix + '-' + self.KubeFullName,
    namespaceName: std.strReplace(self.fullName, '.' + self.shortName, ''),
    prefix: '',
    resourcesQuotas:{
      active: true,
      auto: true,
      pods: Const.limit_pod_concurrent,
      cpuRequest: '%dm' % Const.limit_pod_min_cpu,
      memoryRequest: '%dMi' % Const.limit_pod_min_mem,
      cpuLimit: '%dm' % Const.limit_pod_min_cpu,
      memoryLimit: '%dMi' % Const.limit_pod_min_mem,
    },
    limitRange: true,
    serviceMetrics: true,
    secretsManager: {
      active: false,
      useVaultNamespaceCA: false,
      secretName: 'secretsmanager-ha-tls',
      secretsManagerCaCert: 'secretsmanager.ca',
      env: "",
      secretsManagerServerUrl: "",
      secretsManagerAuthRole: "",
      secretsManagerAuthPath: "jwt"
    },
  },
  deployment: {
    active: true,
    resourceCalculation: true,
    namespace: '',
    cluster: std.extVar('cluster'),
    serverUrl: 'https://gitlab.com/',
    registry: 'registry.gitlab.com',
    repository: 'gitlab-org/ci-cd/gitlab-runner-ubi-images',
    imageBuild: 'gitlab-runner-ocp',
    tagBuild: 'amd64-v17.5.3',
    image: '%s/%s/%s:%s' % [self.registry, self.repository, self.imageBuild, self.tagBuild],
    imageHelper: 'gitlab-runner-helper-ocp',
    tagHelper: 'x86_64-v17.5.3',
    helperImage:'%s/%s/%s:%s' % [self.registry, self.repository, self.imageHelper, self.tagHelper],
    servicesActive: false,
    tagListPrefix: "CI_RUNNER_",
    exportTagListAsEnv: true,
    resources:{
      stalePods: 1,
      runner:{
        cpuRequest: Const.runner_cpu_request,
        cpuLimit: Const.runner_cpu_limit,
        memoryLimit: Const.runner_mem_limit,
        memoryRequest: Const.runner_mem_request,
      },
      helper: {
        cpuRequest: Const.helper_cpu_request,
        cpuLimit: Const.helper_cpu_limit,
        memoryRequest: Const.helper_mem_request,
        memoryLimit: Const.helper_mem_limit,
        storageRequest: Const.helper_storage_request,
        storageLimit: Const.helper_storage_limit,
      },
      init: {
        cpuRequest: Const.init_cpu_request,
        cpuLimit: Const.init_cpu_limit,
        memoryRequest: Const.init_mem_request,
        memoryLimit: Const.init_mem_limit,
      },
      build: {
        cpuRequest: Const.build_cpu_request,
        cpuLimit: Const.build_cpu_limit,
        memoryRequest: Const.build_mem_request,
        memoryLimit: Const.build_mem_limit,
        storageRequest: Const.build_storage_request,
        storageLimit: Const.build_storage_limit,        
      },
      service: {
        cpuRequest: Const.service_cpu_request,
        cpuLimit: Const.service_cpu_limit,
        memoryLimit: Const.service_mem_limit,
        memoryRequest: Const.service_mem_request,
        storageRequest: Const.service_storage_request,
        storageLimit: Const.service_storage_limit,
      },
    },
    probe: {
      liveness: {
        delaySeconds: 480,
        periodSeconds: 30,
        initialDelaySeconds: 60,
        timeoutSeconds: 30,
        successThreshold: 1,
        failureThreshold: 3,
      },
      readiness: {
        initialDelaySeconds: 10,
        delaySeconds: 1,
        periodSeconds: 5,
        timeoutSeconds: 10,
        successThreshold: 1,
        failureThreshold: 3,
      },
    },
    runner:{
      global: {
        concurrent: Const.build_concurrent,  # Limits how many jobs can run concurrently, across all registered runners
        sentryDsn: '',    # Enables tracking of all system level errors to Sentry.
        checkInterval: 10,
        metricsPort: 9252,
      },
      register:{ # https://docs.gitlab.com/ee/api/users.html#create-a-runner-linked-to-a-user
        description:'',
        paused:'',                                                # Set Runner to be paused, defaults to 'false' [$REGISTER_PAUSED]
        locked:'',                                                # Lock Runner for current project, defaults to 'true' [$REGISTER_LOCKED]
        runUntagged:'',                                           # Register to run untagged builds; defaults to 'true' when 'tag-list' is empty [$REGISTER_RUN_UNTAGGED]
        tagList:'',                                               # Tag list [$RUNNER_TAG_LIST]
        accessLevel:'',                                           # Set access_level of the runner to not_protected or ref_protected; defaults to not_protected [$REGISTER_ACCESS_LEVEL]
        maximumTimeout:'',                                        # What is the maximum timeout (in seconds) that will be set for job when using this Runner (default: '0') [$REGISTER_MAXIMUM_TIMEOUT]
        maintenanceNote:'',                                       # Runner's maintenance note [$REGISTER_MAINTENANCE_NOTE]
      },
      runners:{
        env:{}, # ex: "TMPDIR=$CI_PROJECT_DIR.tmp", "HOME=$CI_BUILDS_DIR", "TRANSFER_METER_FREQUENCY=5s"
        kubernetes:{
          volumes:{
            // ex: repo and cache as tmpfs
            // empty_dir:[{
            //     name:'repo',
            //     mount_path: $.deployment.runner.env.runner.buildsDir,
            //     medium: 'Memory'
            //   },{
            //     name:'cache',
            //     mount_path: $.deployment.runner.env.runner.cacheDir,
            //     medium: 'Memory'
            //   }
            // ],
            // ex: add volume to config.toml
            // host_path: [{
            //   name:"hostpath-1",
            //   mount_path: "/path/to/mount/point",
            //   read_only: true,
            //   host_path:"/path/on/host"
            // },{
            //   name:"hostpath-2",
            //   mount_path: "/path/to/mount/point2",
            //   read_only: true,
            //   host_path:"/path/on/host2"
            // },
            // ],
            // pvc: [{
            //   name:"hostpath-2",
            //   mount_path: "/path/to/mount/point2"
            // },
            // ],
          },
          podAnnotations: {
            'gitlab/runner_version':'${CI_RUNNER_VERSION}',
            'gitlab/project_name':'${CI_PROJECT_NAME}',
            'gitlab/project_path':'${CI_PROJECT_PATH_SLUG}',
            'gitlab/job_id':'${CI_JOB_ID}',
            'gitlab/job_url':'$CI_JOB_URL',
            'gitlab/job_name':'${CI_JOB_NAME}',
            'gitlab/job_image':'${CI_JOB_IMAGE}',
            'gitlab/stage_name':'${CI_JOB_STAGE}',
            'gitlab/pipeline_id':'${CI_PIPELINE_ID}',
            'gitlab/pipeline_iid':'${CI_PIPELINE_IID}',
            'gitlab/branch_name':'${CI_BRANCH_NAME}',
            'gitlab/commit_author':'${CI_COMMIT_AUTHOR}',
            'gitlab/job_author':'${GITLAB_USER_NAME}',      
            'gitlab/environment_name':'${CI_ENVIRONMENT_NAME}',
            'gitlab/environment_tier':'${CI_ENVIRONMENT_TIER}',
            'gitlab/environment_slug':'${CI_ENVIRONMENT_SLUG}',
          },
          podLabels: {
            'gitlab/project_name':'${CI_PROJECT_NAME}',
            'gitlab/project_path':'${CI_PROJECT_PATH_SLUG}',
          },
          // nodeSelector: {
          // },
          // nodeTolerations: {
          // },
          // dns_config:{       
          //   // https://docs.gitlab.com/runner/executors/kubernetes.html#pods-dns-config
          //   nameservers: [
          //     "1.2.3.4",
          //   ],
          //   searches: [
          //     "ns1.svc.cluster-domain.example",
          //     "my.dns.search.suffix",
          //   ]     
          // },
          // sessionServer: {
          //   # https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-session_server-section
          //   sessionTimeout: 1800,
          //   internalPort:8093,
          //   externalPort:9000,
          // },
        },
      },      
      env:{
        config:{
          configFile:'',                                            # Config file (default: '/etc/gitlab-runner/config.toml') [$CONFIG_FILE]
          templateConfigFile:'/configmaps/config.template.toml',    # Path to the configuration template file [$TEMPLATE_CONFIG_FILE]
          registrationToken: '',                                    # Runner's registration token [$REGISTRATION_TOKEN]
          logLevel: 'info',                                       # Defines the log level. Options are debug, info, warn, error, fatal, and panic. 
          logFormat: 'runner',                                     # Specifies the log format. Options are runner, text, and json. 
          listenAddress: ':9252',                                  # Defines an address (<host>:<port>) the Prometheus metrics HTTP server should listen on.
        },
        register:{
          debug: 'false',
          // nonInteractive:'true',                                    # un registration unattended [$REGISTER_NON_INTERACTIVE]
          // leaveRunner:'',                                           # Don't remove runner if registration fails [$REGISTER_LEAVE_RUNNER]
          // runUntagged:'',                                           # Register to run untagged builds; defaults to 'true' when 'tag-list' is empty [$REGISTER_RUN_UNTAGGED]
          // locked:'true',                                            # Lock Runner for current project, defaults to 'true' [$REGISTER_LOCKED]
          // accessLevel:'not_protected',                              # Set access_level of the runner to not_protected or ref_protected; defaults to not_protected [$REGISTER_ACCESS_LEVEL]
          // maximumTimeout:'',                                        # What is the maximum timeout (in seconds) that will be set for job when using this Runner (default: '0') [$REGISTER_MAXIMUM_TIMEOUT]
          // paused:'',                                                # Set Runner to be paused, defaults to 'false' [$REGISTER_PAUSED]
          // maintenanceNote:'',                                       # Runner's maintenance note [$REGISTER_MAINTENANCE_NOTE]
        },
        ci:{
          serverUrl: $.deployment.serverUrl,                        # Runner URL [$CI_SERVER_URL]
          token:'',                                                 # Runner token [$CI_SERVER_TOKEN]
          tlsCaFile:'',                                             # File containing the certificates to verify the peer when using HTTPS [$CI_SERVER_TLS_CA_FILE]
          tlsCertFile:'',                                           # File containing certificate for TLS client auth when using HTTPS [$CI_SERVER_TLS_CERT_FILE]
          tlsKeyFile:'',                                            # File containing private key for TLS client auth when using HTTPS [$CI_SERVER_TLS_KEY_FILE]
          debugTraceDisabled:'',                                    # When set to true Runner will disable the possibility of using the CI_DEBUG_TRACE feature [$RUNNER_DEBUG_TRACE_DISABLED]
        },
        runner: {
          debug: 'false',                                           # introduce in gitlab runner 15.2
          tagList: '',                                              # Tag list [$RUNNER_TAG_LIST]
          leaveRunner:'',                                           # Don't remove runner if registration fails [$REGISTER_LEAVE_RUNNER]
          name: '',                                                 # Runner name (default: 'a6606da87d8b') [$RUNNER_NAME]
          limit:'',                                                 # Maximum number of builds processed by this runner (default: '0') [$RUNNER_LIMIT]
          outputLimit:'4096',                                       # Maximum build trace size in kilobytes (default: '0') [$RUNNER_OUTPUT_LIMIT]
          requestConcurrency:'',                                    # Maximum concurrency for job requests (defaul: '0') [$RUNNER_REQUEST_CONCURRENCY]
          unhealthyRequestsLimit:'',                                # The number of 'unhealthy' responses to new job requests after which a runner worker will be disabled (default: "0") [$RUNNER_UNHEALTHY_REQUESTS_LIMIT]
          executor:'kubernetes',                                    # Select executor, eg. shell, docker, etc. [$RUNNER_EXECUTOR]
          buildsDir:'',                                             # Directory where builds are stored [$RUNNER_BUILDS_DIR]
          cacheDir:'',                                              # Directory where build cache is stored [$RUNNER_CACHE_DIR]
          env:'',                                                   # Custom environment variables injected to build environment [$RUNNER_ENV]
          preCloneScript:'',                                        # [DEPRECATED] Use pre_get_sources_script instead [$RUNNER_PRE_CLONE_SCRIPT]
          postCloneScript:'',                                       # [DEPRECATED] Use post_get_sources_script instead [$RUNNER_POST_CLONE_SCRIPT]
          preGetSourcesScript:"",                                   # Runner-specific commands to be executed on the runner before updating the Git repository an updating submodules. [$RUNNER_PRE_GET_SOURCES_SCRIPT]
          postGetSourcesScript:"",                                  # Runner-specific commands to be executed on the runner after updating the Git repository and updating submodules. [$RUNNER_POST_GET_SOURCES_SCRIPT]
          preBuildScript:'',                                        # Runner-specific command script executed just before build executes [$RUNNER_PRE_BUILD_SCRIPT]
          postBuildScript:'',                                       # Runner-specific command script executed just after build executes [$RUNNER_POST_BUILD_SCRIPT]
          debugTraceDisabled:'',                                    # When set to true Runner will disable the possibility of using the CI_DEBUG_TRACE feature [$RUNNER_DEBUG_TRACE_DISABLED]
          safeDirectoryCheckout:'',                                 # When set to true, Git global configuration will get a safe.directory directive pointing the job's working directory' [$RUNNER_SAFE_DIRECTORY_CHECKOUT]
          shell:'',                                                 # Select bash, sh, cmd, pwsh or powershell [$RUNNER_SHELL]
        
        },
        kubernetes: {
          host:'',                                                  # Optional Kubernetes master host URL (auto-discovery attempted if not specified) [$KUBERNETES_HOST]
          certFile:'',                                              # Optional Kubernetes master auth certificate [$KUBERNETES_CERT_FILE]
          keyFile:'',                                               # Optional Kubernetes master auth private key [$KUBERNETES_KEY_FILE]
          caFile:'',                                                # Optional Kubernetes master auth ca certificate [$KUBERNETES_CA_FILE]
          bearerTokenOverwriteAllowed:'',                           # Bool to authorize builds to specify their own bearer token for creation. [$KUBERNETES_BEARER_TOKEN_OVERWRITE_ALLOWED]
          bearerToken:'',                                           # Optional Kubernetes service account token used to start build pods. [$KUBERNETES_BEARER_TOKEN]
          image:'',                                                 # Default docker image to use for builds when none is specified [$KUBERNETES_IMAGE]
          namespace: $.project.kubeNamespaceName,                   # Namespace to run Kubernetes jobs in [$KUBERNETES_NAMESPACE]
          namespaceOverwriteAllowed:'',                             # Regex to validate 'KUBERNETES_NAMESPACE_OVERWRITE' value [$KUBERNETES_NAMESPACE_OVERWRITE_ALLOWED]
          privileged:'false',                                       # Run all containers with the privileged flag enabled [$KUBERNETES_PRIVILEGED]
          runtimeClassName:'',                                      # A Runtime Class to use for all created pods, errors if the feature is unsupported by the cluster [$KUBERNETES_RUNTIME_CLASS_NAME]
          allowPrivilegeEscalation:'',                              # Run all containers with the security context allowPrivilegeEscalation flag enabled. When empty, it does not define the allowPrivilegeEscalation flag in the container SecurityContext and allows Kubernetes to use the default privilege escalation behavior. [$KUBERNETES_ALLOW_PRIVILEGE_ESCALATION]
          cpuLimit: '',                                             # The CPU allocation given to build containers [$KUBERNETES_CPU_LIMIT]
          cpuLimitOverwriteMaxAllowed: '',                           # If set, the max amount the cpu limit can be set to. Used with the KUBERNETES_CPU_LIMIT variable in the build. [$KUBERNETES_CPU_LIMIT_OVERWRITE_MAX_ALLOWED]
          cpuRequest: '',                                           # The CPU allocation requested for build containers [$KUBERNETES_CPU_REQUEST]
          cpuRequestOverwriteMaxAllowed: '',                         # If set, the max amount the cpu request can be set to. Used with the KUBERNETES_CPU_REQUEST variable in the build. [$KUBERNETES_CPU_REQUEST_OVERWRITE_MAX_ALLOWED]
          memoryLimit: '',                                          # The amount of memory allocated to build containers [$KUBERNETES_MEMORY_LIMIT]
          memoryLimitOverwriteMaxAllowed: '',                        # If set, the max amount the memory limit can be set to. Used with the KUBERNETES_MEMORY_LIMIT variable in the build. [$KUBERNETES_MEMORY_LIMIT_OVERWRITE_MAX_ALLOWED]
          memoryRequest: '',                                        # The amount of memory requested from build containers [$KUBERNETES_MEMORY_REQUEST]
          memoryRequestOverwriteMaxAllowed: '',                      # If set, the max amount the memory request can be set to. Used with the KUBERNETES_MEMORY_REQUEST variable in the build. [$KUBERNETES_MEMORY_REQUEST_OVERWRITE_MAX_ALLOWED]
          ephemeralStorageLimit: '',                                # The amount of ephemeral storage allocated to build containers [$KUBERNETES_EPHEMERAL_STORAGE_LIMIT]
          ephemeralStorageLimitOverwriteMaxAllowed:'',              # If set, the max amount the ephemeral limit can be set to. Used with the KUBERNETES_EPHEMERAL_STORAGE_LIMIT variable in the build. [$KUBERNETES_EPHEMERAL_STORAGE_LIMIT_OVERWRITE_MAX_ALLOWED]
          ephemeralStorageRequest: '',                              # The amount of ephemeral storage requested from build containers [$KUBERNETES_EPHEMERAL_STORAGE_REQUEST]
          ephemeralStorageRequestOverwriteMaxAllowed:'',            # If set, the max amount the ephemeral storage request can be set to. Used with the KUBERNETES_EPHEMERAL_STORAGE_REQUEST variable in the build. [$KUBERNETES_EPHEMERAL_STORAGE_REQUEST_OVERWRITE_MAX_ALLOWED]
          serviceCpuLimit: '',                                      # The CPU allocation given to build service containers [$KUBERNETES_SERVICE_CPU_LIMIT]
          serviceCpuLimitOverwriteMaxAllowed:'',                    # If set, the max amount the service cpu limit can be set to. Used with the KUBERNETES_SERVICE_CPU_LIMIT variable in the build. [$KUBERNETES_SERVICE_CPU_LIMIT_OVERWRITE_MAX_ALLOWED]
          serviceCpuRequest: '',                                    # The CPU allocation requested for build service containers [$KUBERNETES_SERVICE_CPU_REQUEST]
          serviceCpuRequestOverwriteMaxAllowed:'',                  # If set, the max amount the service cpu request can be set to. Used with the KUBERNETES_SERVICE_CPU_REQUEST variable in the build. [$KUBERNETES_SERVICE_CPU_REQUEST_OVERWRITE_MAX_ALLOWED]
          serviceMemoryLimit: '',                                   # The amount of memory allocated to build service containers [$KUBERNETES_SERVICE_MEMORY_LIMIT]
          serviceMemoryLimitOverwriteMaxAllowed:'',                 # If set, the max amount the service memory limit can be set to. Used with the KUBERNETES_SERVICE_MEMORY_LIMIT variable in the build. [$KUBERNETES_SERVICE_MEMORY_LIMIT_OVERWRITE_MAX_ALLOWED]
          serviceMemoryRequest: '',                                 # The amount of memory requested for build service containers [$KUBERNETES_SERVICE_MEMORY_REQUEST]
          serviceMemoryRequestOverwriteMaxAllowed:'',               # If set, the max amount the service memory request can be set to. Used with the KUBERNETES_SERVICE_MEMORY_REQUEST variable in the build. [$KUBERNETES_SERVICE_MEMORY_REQUEST_OVERWRITE_MAX_ALLOWED]
          serviceEphemeralStorageLimit: '',                         # The amount of ephemeral storage allocated to build service containers [$KUBERNETES_SERVICE_EPHEMERAL_STORAGE_LIMIT]
          serviceEphemeralStorageLimitOverwriteMaxAllowed:'',       # If set, the max amount the service ephemeral storage limit can be set to. Used with the KUBERNETES_SERVICE_EPHEMERAL_STORAGE_LIMIT variable in the build. [$KUBERNETES_SERVICE_EPHEMERAL_STORAGE_LIMIT_OVERWRITE_MAX_ALLOWED]
          serviceEphemeralStorageRequest: '',                       # The amount of ephemeral storage requested for build service containers [$KUBERNETES_SERVICE_EPHEMERAL_STORAGE_REQUEST]
          serviceEphemeralStorageRequestOverwriteMaxAllowed:'',     # If set, the max amount the service ephemeral storage request can be set to. Used with the KUBERNETES_SERVICE_EPHEMERAL_STORAGE_REQUEST variable in the build. [$KUBERNETES_SERVICE_EPHEMERAL_STORAGE_REQUEST_OVERWRITE_MAX_ALLOWED]
          helperCpuLimit: '',                                       # The CPU allocation given to build helper containers [$KUBERNETES_HELPER_CPU_LIMIT]
          helperCpuLimitOverwriteMaxAllowed:'',                     # If set, the max amount the helper cpu limit can be set to. Used with the KUBERNETES_HELPER_CPU_LIMIT variable in the build. [$KUBERNETES_HELPER_CPU_LIMIT_OVERWRITE_MAX_ALLOWED]
          helperCpuRequest: '',                                     # The CPU allocation requested for build helper containers [$KUBERNETES_HELPER_CPU_REQUEST]
          helperCpuRequestOverwriteMaxAllowed:'',                   # If set, the max amount the helper cpu request can be set to. Used with the KUBERNETES_HELPER_CPU_REQUEST variable in the build. [$KUBERNETES_HELPER_CPU_REQUEST_OVERWRITE_MAX_ALLOWED]
          helperMemoryLimit: '',                                    # The amount of memory allocated to build helper containers [$KUBERNETES_HELPER_MEMORY_LIMIT]
          helperMemoryLimitOverwriteMaxAllowed:'',                  # If set, the max amount the helper memory limit can be set to. Used with the KUBERNETES_HELPER_MEMORY_LIMIT variable in the build. [$KUBERNETES_HELPER_MEMORY_LIMIT_OVERWRITE_MAX_ALLOWED]
          helperMemoryRequest: '',                                  # The amount of memory requested for build helper containers [$KUBERNETES_HELPER_MEMORY_REQUEST]
          helperMemoryRequestOverwriteMaxAllowed:'',                # If set, the max amount the helper memory request can be set to. Used with the KUBERNETES_HELPER_MEMORY_REQUEST variable in the build. [$KUBERNETES_HELPER_MEMORY_REQUEST_OVERWRITE_MAX_ALLOWED]
          helperEphemeralStorageLimit: '',                          # The amount of ephemeral storage allocated to build helper containers [$KUBERNETES_HELPER_EPHEMERAL_STORAGE_LIMIT]
          helperEphemeralStorageLimitOverwriteMaxAllowed:'',        # If set, the max amount the helper ephemeral storage limit can be set to. Used with the KUBERNETES_HELPER_EPHEMERAL_STORAGE_LIMIT variable in the build. [$KUBERNETES_HELPER_EPHEMERAL_STORAGE_LIMIT_OVERWRITE_MAX_ALLOWED]
          helperEphemeralStorageRequest: '',                        # The amount of ephemeral storage requested for build helper containers [$KUBERNETES_HELPER_EPHEMERAL_STORAGE_REQUEST]
          helperEphemeralStorageRequestOverwriteMaxAllowed:'',      # If set, the max amount the helper ephemeral storage request can be set to. Used with the KUBERNETES_HELPER_EPHEMERAL_STORAGE_REQUEST variable in the build. [$KUBERNETES_HELPER_EPHEMERAL_STORAGE_REQUEST_OVERWRITE_MAX_ALLOWED]
          allowedImages:'',                                         # Image allowlist [$KUBERNETES_ALLOWED_IMAGES]
          allowedServices:'',                                       # Service allowlist [$KUBERNETES_ALLOWED_SERVICES]
          pullPolicy:'if-not-present',                              # Policy for if/when to pull a container image (never, if-not-present, always). The cluster default will be used if not set [$KUBERNETES_PULL_POLICY]
          nodeSelector:'',                                          # A toml table/json object of key:value. Value is expected to be a string. When set this will create pods on k8s nodes that match all the key:value pairs. Only one selector is supported through environment variable configuration. (default: '{}') [$KUBERNETES_NODE_SELECTOR]
          nodeTolerations:'',                                       # A toml table/json object of key=value:effect. Value and effect are expected to be strings. When set, pods will tolerate the given taints. Only one toleration is supported through environment variable configuration. (default: '{}') [$KUBERNETES_NODE_TOLERATIONS]
          imagePullSecrets:'',                                      # A list of image pull secrets that are used for pulling docker image [$KUBERNETES_IMAGE_PULL_SECRETS]
          helperImage: $.deployment.helperImage,                    # [ADVANCED] Override the default helper image used to clone repos and upload artifacts [$KUBERNETES_HELPER_IMAGE]
          helperImageFlavor:'',                                     # Set helper image flavor (alpine, ubuntu), defaults to alpine [$KUBERNETES_HELPER_IMAGE_FLAVOR]
          terminationGracePeriodSeconds:'',                         # Duration after the processes running in the pod are sent a termination signal and the time when the processes are forcibly halted with a kill signal.DEPRECATED: use KUBERNETES_POD_TERMINATION_GRACE_PERIOD_SECONDS and KUBERNETES_CLEANUP_GRACE_PERIOD_SECONDS instead. [$KUBERNETES_TERMINATIONGRACEPERIODSECONDS]
          podTerminationGracePeriodSeconds:'',                      # Pod-level setting which determines the duration in seconds which the pod has to terminate gracefully. After this, the processes are forcibly halted with a kill signal. Ignored if KUBERNETES_TERMINATIONGRACEPERIODSECONDS is specified. [$KUBERNETES_POD_TERMINATION_GRACE_PERIOD_SECONDS]
          cleanupGracePeriodSeconds:'',                             # When cleaning up a pod on completion of a job, the duration in seconds which the pod has to terminate gracefully. After this, the processes are forcibly halted with a kill signal. Ignored if KUBERNETES_TERMINATIONGRACEPERIODSECONDS is specified. [$KUBERNETES_CLEANUP_GRACE_PERIOD_SECONDS]
          pollInterval:'',                                          # How frequently, in seconds, the runner will poll the Kubernetes pod it has just created to check its status (default: '0') [$KUBERNETES_POLL_INTERVAL]
          pollTimeout:'180',                                        # The total amount of time, in seconds, that needs to pass before the runner will timeout attempting to connect to the pod it has just created (useful for queueing more builds that the cluster can handle at a time) (default: '0') [$KUBERNETES_POLL_TIMEOUT]
          resourceAvailabilityCheckMaxAttempts:'',                  # The maximum number of attempts to check if a resource (service account and/or pull secret) set is available before giving up. There is 5 seconds interval between each attempt (default: '0') [$KUBERNETES_RESOURCE_AVAILABILITY_CHECK_MAX_ATTEMPTS]
          podLabelsOverwriteAllowed:'',                             # Regex to validate 'KUBERNETES_POD_LABELS_*' values [$KUBERNETES_POD_LABELS_OVERWRITE_ALLOWED]
          SchedulerName: '',                                        # Pods will be scheduled using this scheduler, if it exists [$KUBERNETES_SCHEDULER_NAME],
          serviceAccount: $.project.kubeNamespaceName + '-' + Kube.namingServiceAccount, # Executor pods will use this Service Account to talk to kubernetes API [$KUBERNETES_SERVICE_ACCOUNT]
          serviceAccountOverwriteAllowed:'',                        # Regex to validate 'KUBERNETES_SERVICE_ACCOUNT' value [$KUBERNETES_SERVICE_ACCOUNT_OVERWRITE_ALLOWED]
          podAnnotationsOverwriteAllowed:'',                        # Regex to validate 'KUBERNETES_POD_ANNOTATIONS_*' values [$KUBERNETES_POD_ANNOTATIONS_OVERWRITE_ALLOWED]
          podSecurityContextFsGroup:'',                             # A special supplemental group that applies to all containers in a pod [$KUBERNETES_POD_SECURITY_CONTEXT_FS_GROUP]
          podSecurityContextRunAsGroup:'',                          # The GID to run the entrypoint of the container process [$KUBERNETES_POD_SECURITY_CONTEXT_RUN_AS_GROUP]
          podSecurityContextRunAsNonRoot:'',                        # Indicates that the container must run as a non-root user [$KUBERNETES_POD_SECURITY_CONTEXT_RUN_AS_NON_ROOT]
          podSecurityContextRunAsUser:'',                           # The UID to run the entrypoint of the container process [$KUBERNETES_POD_SECURITY_CONTEXT_RUN_AS_USER]
          podSecurityContextSupplementalGroups:'',                  # A list of groups applied to the first process run in each container, in addition to the container's primary GID
          buildContainerSecurityContextCapabilitiesAdd:'',          # List of capabilities to add to the build container [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_ADD
          buildContainerSecurityContextCapabilitiesDrop:'',         # List of capabilities to drop from the build container [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_DROP]
          buildContainerSecurityContextPrivileged:'',               # Run container in privileged mode [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_PRIVILEGED]
          buildContainerSecurityContextRunAsUser:'',                # The UID to run the entrypoint of the container process [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_RUN_AS_USER]
          buildContainerSecurityContextRunAsGroup:'',               # The GID to run the entrypoint of the container process [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_RUN_AS_GROUP]
          buildContainerSecurityContextRunAsNonRoot:'',             # Indicates that the container must run as a non-root user [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_RUN_AS_NON_ROOT]
          buildContainerSecurityContextReadOnlyRootFilesystem:'',   # Whether this container has a read-only root filesystem. [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_READ_ONLY_ROOT_FILESYSTEM]
          buildContainerSecurityContextAllowPrivilegeEscalation:'', # AllowPrivilegeEscalation controls whether a process can gain more privileges than its parent process [$KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_ALLOW_PRIVILEGE_ESCALATION]
          helperContainerSecurityContextCapabilitiesAdd:'',         # List of capabilities to add to the build container [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_ADD]
          helperContainerSecurityContextCapabilitiesDrop:'',        # List of capabilities to drop from the build container [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_DROP]
          helperContainerSecurityContextPrivileged:'',              # Run container in privileged mode [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_PRIVILEGED]
          helperContainerSecurityContextRunAsUser:'',               # The UID to run the entrypoint of the container process [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_RUN_AS_USER]
          helperContainerSecurityContextRunAsGroup:'',              # The GID to run the entrypoint of the container process [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_RUN_AS_GROUP]
          helperContainerSecurityContextRunAsNonRoot:'',            # Indicates that the container must run as a non-root user [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_RUN_AS_NON_ROOT]
          helperContainerSecurityContextReadOnlyRootFilesystem:'',  # Whether this container has a read-only root filesystem. [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_READ_ONLY_ROOT_FILESYSTEM]
          helperContainerSecurityContextAllowPrivilegeEscalation:'',# AllowPrivilegeEscalation controls whether a process can gain more privileges than its parent process [$KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_ALLOW_PRIVILEGE_ESCALATION]
          serviceContainerSecurityContextCapabilitiesAdd:'',        # List of capabilities to add to the build container [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_ADD]
          serviceContainerSecurityContextCapabilitiesDrop:'',       # List of capabilities to drop from the build container [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_DROP]
          serviceContainerSecurityContextPrivileged:'',             # Run container in privileged mode [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_PRIVILEGED]
          serviceContainerSecurityContextRunAsUser:'',              # The UID to run the entrypoint of the container process [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_RUN_AS_USER]
          serviceContainerSecurityContextRunAsGroup:'',             # The GID to run the entrypoint of the container process [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_RUN_AS_GROUP]
          serviceContainerSecurityContextRunAsNonRoot:'',           # Indicates that the container must run as a non-root user [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_RUN_AS_NON_ROOT]
          serviceContainerSecurityContextReadOnlyRootFilesystem:'', # Whether this container has a read-only root filesystem. [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_READ_ONLY_ROOT_FILESYSTEM]
          serviceContainerSecurityContextAllowPrivilegeEscalation:'', # llowPrivilegeEscalation controls whether a process can gain more privileges than its parent process [$KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_ALLOW_PRIVILEGE_ESCALATION]
          hostAliases:'',                                           # Add Linux capabilities [$KUBERNETES_CAP_ADD]
          capAdd:'',                                                # Drop Linux capabilities [$KUBERNETES_CAP_DROP]
          capDrop:'',                                               # Drop Linux capabilities [$KUBERNETES_CAP_DROP]
          dnsPolicy:'',                                             # How Kubernetes should try to resolve DNS from the created pods. If unset, Kubernetes will use the default 'ClusterFirst'. Valid values are: none, default, cluster-first, cluster-first-with-host-net [$KUBERNETES_DNS_POLICY]
          priorityClassName:'',                                     # If set, the Kubernetes Priority Class to be set to the Pods [$KUBERNETES_PRIORITY_CLASS_NAME]
        },
        cache:{
          cacheDir:'',                                              # Directory where build cache is stored [$RUNNER_CACHE_DIR]
          cacheType:'',                                             # Select caching method [$CACHE_TYPE]
          cachePath:'',                                             # Name of the path to prepend to the cache URL [$CACHE_PATH]
          cacheShared:'',                                           # Enable cache sharing between runners. [$CACHE_SHARED]
          cacheS3ServerAddress:'',                                  # A host:port to the used S3-compatible server [$CACHE_S3_SERVER_ADDRESS]
          cacheS3AccessKey:'',                                      # S3 Access Key [$CACHE_S3_ACCESS_KEY]
          cacheS3SecretKey:'',                                      # S3 Secret Key [$CACHE_S3_SECRET_KEY]
          cacheS3BucketName:'',                                     # Name of the bucket where cache will be stored [$CACHE_S3_BUCKET_NAME]
          cacheS3BucketLocation:'',                                 # Name of S3 region [$CACHE_S3_BUCKET_LOCATION]
          cacheS3Insecure:'',                                       # Use insecure mode (without https) [$CACHE_S3_INSECURE]
          cacheS3AuthenticationType:'',                             # IAM or credentials [$CACHE_S3_AUTHENTICATION_TYPE]
          cacheGcsAccessId:'',                                      # ID of GCP Service Account used to access the storage [$CACHE_GCS_ACCESS_ID]
          cacheGcsPrivateKey:'',                                    # Private key used to sign GCS requests [$CACHE_GCS_PRIVATE_KEY]
          cacheGcsBucketName:'',                                    # Name of the bucket where cache will be stored [$CACHE_GCS_BUCKET_NAME]
          cacheAzureAccountName:'',                                 # Account name for Azure Blob Storage [$CACHE_AZURE_ACCOUNT_NAME]
          cacheAzureAccountKey:'',                                  # Access key for Azure Blob Storage [$CACHE_AZURE_ACCOUNT_KEY]
          cacheAzureContainerName:'',                               # Name of the Azure container where cache will be stored [$CACHE_AZURE_CONTAINER_NAME]
          cacheAzureStorageDomain:'',                               # Domain name of the Azure storage (e.g. blob.core.windows.net) [$CACHE_AZURE_STORAGE_DOMAIN]
        },
        custom: {
          customBuildDirEnabled:'',                                 # Enable job specific build directories [$CUSTOM_BUILD_DIR_ENABLED]
          customConfigExec:'',                                      # Executable that allows to inject configuration values to the executor [$CUSTOM_CONFIG_EXEC]
          customConfigExecTimeout:'',                               # Timeout for the config executable (in seconds) [$CUSTOM_CONFIG_EXEC_TIMEOUT]
          customPrepareExec:'',                                     # Executable that prepares executor [$CUSTOM_PREPARE_EXEC]
          customPrepareExecTimeout:'',                              # Timeout for the prepare executable (in seconds) [$CUSTOM_PREPARE_EXEC_TIMEOUT]
          customRunExec:'',                                         # Executable that runs the job script in executor [$CUSTOM_RUN_EXEC]
          customCleanupExec:'',                                     # Executable that cleanups after executor run [$CUSTOM_CLEANUP_EXEC]
          customCleanupExecTimeout:'',                              # Timeout for the cleanup executable (in seconds) [$CUSTOM_CLEANUP_EXEC_TIMEOUT]
          customGracefulKillTimeout:'',                             # Graceful timeout for scripts execution after SIGTERM is sent to the process (in seconds). This limits the time given for scripts to perform the cleanup before exiting [$CUSTOM_GRACEFUL_KILL_TIMEOUT]
          customForceKillTimeout:'',                                # Force timeout for scripts execution (in seconds). Counted from the force kill call; if process will be not terminated, Runner will abandon process termination and log an error [$CUSTOM_FORCE_KILL_TIMEOUT]
        }    
      },
    },
    persistentVolumeClaim:{
      active:false,
      isGlobalDefinition: false,
      definition: {
        storageClassName: '',
        accessModes: ['ReadWriteMany'],
        resources: {
          requests: {
            storage: '20Gi',
          },
        },
        //volumeName: $.project.kubeNamespaceName + '-' + Kube.namingPersistentVolume
      },
      persistentVolume: {
        active:false,
        definition: {
          storageClassName: '',
          capacity: {
            storage: '20Gi',
          },
          accessModes: ['ReadWriteMany'],
          persistentVolumeReclaimPolicy: 'Delete', # 'Retain',
          claimRef: {
            namespace: $.project.kubeNamespaceName,
            name: $.project.kubeNamespaceName + '-' + Kube.namingPersistentVolumeClaim,
          },
          nfs: {
            server: '',
            path: '',
            readOnly: false,
          },
          mountOptions: [
            "vers=4.2", "rw", "proto=tcp", "rsize=32768", "wsize=32768", "timeo=600", "fg", "hard", "retrans=10", "intr", "relatime", "nodiratime", "async"
          ],
        }
      },      
    },  
  },  
}
