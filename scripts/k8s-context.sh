#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"

LOCAL_CONFIG="${HOME}/.cbi/config"
KUBE_CONFIG="$HOME/.kube/config"

if [[ ! -f "${LOCAL_CONFIG}" ]]; then
  log warn "File '${LOCAL_CONFIG}' does not exists \n" \
  "Create one to configure the location of the kubeconfig file and the associated context. Example:\n" \
  '{"kubeconfig": { "path": "~/.kube/config"}}' | jq -M

else
  if ! jq -e '.kubeconfig.path' <"${LOCAL_CONFIG}" > /dev/null; then
    log warn "File '${LOCAL_CONFIG}' does not contain proper configuration\n" \
    "Create one to configure the location of the kubeconfig file and the associated context. Example:\n" \
    '{"kubeconfig": { "path": "~/.kube/config"}}' | jq -M
  else
    KUBE_CONFIG="$(jq -r '.kubeconfig.path' <"${LOCAL_CONFIG}")"
    KUBE_CONFIG="$(readlink -f "${KUBE_CONFIG/#~\//${HOME}/}")"
  fi
fi

if [[ ! -f "${KUBE_CONFIG}" ]]; then
  log error "Cannot find kubeconfig file '${KUBE_CONFIG}'"
  exit 2
fi

export KUBE_CONFIG

CURRENT_CLUSTER_CONTEXT="$(kubectl config current-context)"

log warn "Current cluster context: ${CURRENT_CLUSTER_CONTEXT}"
