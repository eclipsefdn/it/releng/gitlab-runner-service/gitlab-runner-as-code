#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# GitLab admin functions

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'

check_parameter() {
  local param_name="${1:-}"
  local param="${2:-}"
  # check that parameter is not empty
  if [[ -z "${param}" ]] || [[ "${param}" == "null" ]]; then
    printf "ERROR: a %s must be given.\n" "${param_name}"
    exit 1
  fi
}

urlencode() {
  local S="${1}"
  local encoded=""
  local ch
  local o
  for i in $(seq 0 $((${#S} - 1)) )
  do
      ch=${S:$i:1}
      case "${ch}" in
          [-_.~a-zA-Z0-9]) 
              o="${ch}"
              ;;
          *) 
              o=$(printf '%%%02x' "'$ch")                
              ;;
      esac
      encoded="${encoded}${o}"
  done
  echo "${encoded}"
}
