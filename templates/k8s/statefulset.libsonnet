// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
{
  gen(config, configctx, ctx): Kube.StatefulSet(config.project.shortName, config, ctx) {
    local kubeNamespaceNameCtx = config.project.shortName + '-' + ctx + '-' + Kube.namingStatefulSet,
    local transformKey(obj, f) =
      [
        if obj[x] != '' then
          { name: f(x) }
          { value: obj[x] }
        for x in std.objectFieldsAll(obj)
      ],

    local toCamelCase(str_) =
      local str = std.toString(str_);
      local trans(ch) =
        local cp = std.codepoint(ch);
        if (cp >= 65 && cp <= 90) then
          '_' + std.char(cp + 32)
        else
          ch;
      '%s' % std.join('', [trans(ch) for ch in std.stringChars(str)]),

    spec: {
      replicas: 1,
      revisionHistoryLimit: 10,
      selector: {
        matchLabels: Kube.gracLabels(config, ctx),
      },
      serviceName: kubeNamespaceNameCtx,
      template: {
        metadata: {
          labels: Kube.gracLabels(config, ctx),
          name: kubeNamespaceNameCtx,
          annotations: {
            'prometheus.io/scrape': 'true',
            'prometheus.io/port': '9252',
          },
        },
        spec: {
          serviceAccountName: config.project.kubeNamespaceName + '-' + Kube.namingServiceAccount,
          containers: [
            {
              name: 'gitlab-runner',
              image: configctx.image,
              imagePullPolicy: 'IfNotPresent',
              securityContext: {
                allowPrivilegeEscalation: false,
                capabilities: {
                  drop: [
                    'ALL',
                  ],
                },
                privileged: false,
                readOnlyRootFilesystem: false,
                runAsNonRoot: true,
                // fsGroup: 65533,
                // runAsGroup: 65533,
                // runAsUser: 1000750001, // Range change to 1000790000.
                // supplementalGroups: [65533]
              },
              command: ["/usr/local/bin/tini", "--", "/bin/bash", "/configmaps/entrypoint"],
              env: std.prune(std.filter(function(x) x != null && std.findSubstr('_GRAC_', x.name) == [], std.flattenArrays([
                if std.objectHas(configctx.runner.env, 'config') then transformKey(configctx.runner.env.config, function(x) std.asciiUpper(toCamelCase(x))),
                if std.objectHas(configctx.runner.env, 'register') then transformKey(configctx.runner.env.register, function(x) 'REGISTER_' + std.asciiUpper(toCamelCase(x))),
                if std.objectHas(configctx.runner.env, 'ci') then transformKey(configctx.runner.env.ci, function(x) 'CI_' + std.asciiUpper(toCamelCase(x))),
                if std.objectHas(configctx.runner.env, 'runner') then transformKey(configctx.runner.env.runner, function(x) 'RUNNER_' + std.asciiUpper(toCamelCase(x))),
                if std.objectHas(configctx.runner.env, 'kubernetes') then transformKey(configctx.runner.env.kubernetes, function(x) 'KUBERNETES_' + std.asciiUpper(toCamelCase(x))),
                if std.objectHas(configctx.runner.env, 'cache') then transformKey(configctx.runner.env.cache, function(x) 'CACHE_' + std.asciiUpper(toCamelCase(x))),
                if std.objectHas(configctx.runner.env, 'custom') then transformKey(configctx.runner.env.custom, function(x) 'CUSTOM_' + std.asciiUpper(toCamelCase(x))),
              ]))) + std.prune([
                {
                  name: 'NODE_NAME',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'spec.nodeName',
                    },
                  },
                },
                {
                  name: 'POD_NAME',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'metadata.name',
                    },
                  },
                },
                {
                  name: 'POD_NAMESPACE',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'metadata.namespace',
                    },
                  },
                },
                {
                  name: 'POD_IP',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'status.podIP',
                    },
                  },
                },
                {
                  name: 'POD_SERVICE_ACCOUNT',
                  valueFrom: {
                    fieldRef: {
                      fieldPath: 'spec.serviceAccountName',
                    },
                  },
                },
                if (config.project.secretsManager.active && config.project.secretsManager.useVaultNamespaceCA) then
                  {
                    name: 'VAULT_CACERT',
                    value: '/home/gitlab-runner/.gitlab-runner/certs/' + config.project.secretsManager.secretsManagerCaCert,
                  },
              ]),
              livenessProbe: {
                exec: {
                  command: [
                    '/bin/bash',
                    '/configmaps/check-live',
                  ],
                },
                initialDelaySeconds: configctx.probe.liveness.delaySeconds,
                periodSeconds: configctx.probe.liveness.periodSeconds,
                failureThreshold: configctx.probe.liveness.failureThreshold,
                timeoutSeconds: configctx.probe.liveness.timeoutSeconds,
              },
              readinessProbe: {
                exec: {
                  command: [
                    '/usr/bin/pgrep',
                    'gitlab.*runner',
                  ],
                },
                initialDelaySeconds: configctx.probe.readiness.delaySeconds,
                periodSeconds: configctx.probe.readiness.periodSeconds,
                failureThreshold: configctx.probe.readiness.failureThreshold,
                timeoutSeconds: configctx.probe.readiness.timeoutSeconds,
              },
              ports: std.prune([
                if (config.project.serviceMetrics) then
                  {
                    name: std.substr(ctx, 0, 11) + '-mcs',
                    containerPort: configctx.runner.global.metricsPort,
                    protocol: 'TCP',
                  },
                if (std.objectHas(configctx.runner.runners.kubernetes, 'sessionServer')) then
                  {
                    name: std.substr(ctx, 0, 12) + '-ss',
                    containerPort: configctx.runner.runners.kubernetes.sessionServer.internalPort,
                    protocol: 'TCP',
                  },
              ]),
              resources: {
                requests: {
                  [if configctx.resources.runner.cpuRequest != '' then 'cpu']: '%dm' % configctx.resources.runner.cpuRequest,
                  [if configctx.resources.runner.memoryRequest != '' then 'memory']: '%dMi' % configctx.resources.runner.memoryRequest,
                },
                limits: {
                  [if configctx.resources.runner.cpuLimit != '' then 'cpu']: '%dm' % configctx.resources.runner.cpuLimit,
                  [if configctx.resources.runner.memoryLimit != '' then 'memory']: '%dMi' % configctx.resources.runner.memoryLimit,
                },
              },
              volumeMounts: std.prune([
                {
                  name: 'project-secrets',
                  mountPath: '/secrets',
                },
                {
                  name: 'etc-gitlab-runner',
                  mountPath: '/home/gitlab-runner/.gitlab-runner',
                },
                {
                  name: 'configmaps',
                  mountPath: '/configmaps',
                },
                if (config.project.secretsManager.active && config.project.secretsManager.useVaultNamespaceCA) then
                  {
                    name: 'secretsmanager-certs',
                    readOnly: true,
                    mountPath: '/home/gitlab-runner/.gitlab-runner/certs/',
                  },
              ]),
            },
          ],
          volumes: std.prune([
            {
              name: 'runner-secrets',
              emptyDir: {},
            },
            {
              name: 'etc-gitlab-runner',
              emptyDir: {},
            },
            {
              name: 'project-secrets',
              projected: {
                sources: [
                  {
                    secret: {
                      name: config.project.kubeNamespaceName + '-' + ctx + '-' + Kube.namingSecret,
                      items: [
                        {
                          key: 'runner-registration-token',
                          path: 'runner-registration-token',
                        },
                        {
                          key: 'runner-token',
                          path: 'runner-token',
                        },
                      ],
                    },
                  },
                ],
              },
            },
            {
              name: 'configmaps',
              configMap: {
                name: config.project.kubeNamespaceName + '-' + ctx + '-' + Kube.namingConfigMap,
              },
            },
            if (config.project.secretsManager.active && config.project.secretsManager.useVaultNamespaceCA) then
              {
                name: 'secretsmanager-certs',
                secret: {
                  secretName: config.project.secretsManager.secretName,
                },
              },
          ]),
        },
      },
    },
  },
}
