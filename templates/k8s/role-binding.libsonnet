// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
{
  gen(config): Kube.RoleBinding(config.project.kubeNamespaceName, config) {
    roleRef: {
      kind: 'Role',
      name: config.project.kubeNamespaceName + '-' + Kube.namingRole,
    },
    subjects: [
      {
        kind: 'ServiceAccount',
        name: config.project.kubeNamespaceName + '-' + Kube.namingServiceAccount,
        namespace: config.project.kubeNamespaceName,
      },
    ],
  },
}
