// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

{
  namingNs: "",
  namingPersistentVolume: "pv",
  namingPersistentVolumeClaim: "pvc",
  namingLimitRange: "lr",
  namingResourceQuota: "rq",
  namingRoleBinding: "rb",
  namingRole: "role",
  namingRoute: "route",
  namingServiceAccount: "sa",
  namingService: "service",
  namingServiceMetrics: "sm",
  namingServiceSessionServeur: "3s",
  namingConfigMap: "cm",
  namingSecret: "secret",
  namingDeployment: "deploy",
  namingStatefulSet: "sfs",
  
  __:: {
    KubeObject(apiVersion, kind, name, suffix, config):: {
      local this = self,
      apiVersion: apiVersion,
      kind: kind,
      metadata: {
        name: if (suffix == "") then name else name + '-' + suffix,
        labels: $.gracProjectLabels(config),
      },
    },

    KubeNSObject(apiVersion, kind, name, suffix, config, opaque=false):: self.KubeObject(apiVersion, kind, name, suffix, config) + {
      metadata+: {
        namespace: config.project.kubeNamespaceName
      },
      [if opaque then 'type']: 'Opaque',
    },

    KubeNSCtxObject(apiVersion, kind, name, suffix, config, ctx='', opaque=false):: self.KubeObject(apiVersion, kind, name, suffix, config) + {
      metadata+: {
        name: if ctx != '' then name + '-' + ctx + '-' + suffix else name + '-' + suffix,
        namespace: config.project.kubeNamespaceName,
        labels: $.gracLabels(config, ctx),
      },
      [if opaque then 'type']: 'Opaque',
    },
  },

  List(items): {
    apiVersion: 'v1',
    kind: 'List',
    items: [] + items,
  },

  gracProjectLabels(config):: {
    [config.project.organizationFullName + '/project.shortname']: config.project.shortName,
    [config.project.organizationFullName + '/project.fullname']: config.project.fullName,
    [config.project.organizationFullName + '/project.namespace']: config.project.namespaceName,
  },

  gracLabels(config, ctx):: self.gracProjectLabels(config) + {
    [if ctx != '' then config.project.organizationFullName + '/project.ctx']: ctx
  },

  Namespace(name, config): $.__.KubeObject('v1', 'Namespace', name, self.namingNs, config),
  PersistentVolume(name, config, ctx): $.__.KubeNSCtxObject("v1", "PersistentVolume", name, self.namingPersistentVolume, config, ctx),
  PersistentVolumeClaim(name, config, ctx): $.__.KubeNSCtxObject("v1", "PersistentVolumeClaim", name, self.namingPersistentVolumeClaim, config, ctx),
  LimitRange(name, config): $.__.KubeNSObject('v1', 'LimitRange', name, self.namingLimitRange, config),
  ResourceQuota(name, config): $.__.KubeNSObject('v1', 'ResourceQuota', name, self.namingResourceQuota, config),
  RoleBinding(name, config): $.__.KubeNSObject('rbac.authorization.k8s.io/v1', 'RoleBinding', name, self.namingRoleBinding, config),
  Role(name, config): $.__.KubeNSObject('rbac.authorization.k8s.io/v1', 'Role', name, self.namingRole, config),
  Route(name, config): $.__.KubeNSObject('route.openshift.io/v1', 'Route', name, self.namingRoute, config),
  ServiceAccount(name, config): $.__.KubeNSObject('v1', 'ServiceAccount', name, self.namingServiceAccount, config),
  Service(name, config): $.__.KubeNSObject('v1', 'Service', name, self.namingService, config),
  ServiceMetrics(name, config, ctx): $.__.KubeNSCtxObject('v1', 'Service', name, self.namingServiceMetrics, config, ctx),
  ServiceSessionServer(name, config): $.__.KubeNSCtxObject('v1', 'Service', name, self.namingServiceSessionServeur, config),
  ConfigMap(name, config): $.__.KubeNSObject('v1', 'ConfigMap', name, self.namingConfigMap, config),
  Secret(name, config, ctx): $.__.KubeNSCtxObject('v1', 'Secret', name, self.namingSecret, config, ctx, opaque=true),
  Deployment(name, config, ctx): $.__.KubeNSCtxObject('apps/v1', 'Deployment', name, self.namingDeployment, config, ctx),
  StatefulSet(name, config, ctx): $.__.KubeNSCtxObject('apps/v1', 'StatefulSet', name, self.namingStatefulSet, config, ctx),

  stripSI(n):: (
    if std.isString(n) then
      local suffix_len =
        if std.endsWith(n, 'm') then 1
        else if std.endsWith(n, 'K') then 1
        else if std.endsWith(n, 'M') then 1
        else if std.endsWith(n, 'G') then 1
        else if std.endsWith(n, 'T') then 1
        else if std.endsWith(n, 'P') then 1
        else if std.endsWith(n, 'E') then 1
        else if std.endsWith(n, 'Ki') then 2
        else if std.endsWith(n, 'Mi') then 2
        else if std.endsWith(n, 'Gi') then 2
        else if std.endsWith(n, 'Ti') then 2
        else if std.endsWith(n, 'Pi') then 2
        else if std.endsWith(n, 'Ei') then 2
        else error 'Unknown numerical suffix in ' + n;
      local n_len = std.length(n);
      std.parseInt(std.substr(n, 0, n_len - suffix_len))
    else 
      (n)
  ),

  pair_list_ex(tab, kfield, vfield)::
    [{ [kfield]: k, [vfield]: tab[k] } for k in std.objectFields(tab)],

  pair_list(tab)::
    self.pair_list_ex(tab, 'name', 'value'),
  
  max2(a,b):: 
    std.max($.stripSI(a),$.stripSI(b)),

  max3(a,b,c)::
    $.max2(a,$.max2(b,c)),

  max4(a,b,c,d)::
    $.max2(a,$.max3(b,c,d)),

  max5(a,b,c,d,e)::
    $.max2(a,$.max4(b,c,d,e)),

  min2(a,b):: 
    std.min($.stripSI(a),$.stripSI(b)),

  min3(a,b,c)::
    $.min2(a,$.min2(b,c)),

  min4(a,b,c,d)::
    $.min2(a,$.min3(b,c,d)),

  min5(a,b,c,d,e)::
    $.min2(a,$.min4(b,c,d,e)),

  add2(a,b):: 
    $.stripSI(a) + $.stripSI(b),

  add3(a,b,c)::
    $.add2(a,$.add2(b,c)),

  add4(a,b,c,d)::
    $.add2(a,$.add3(b,c,d)),
}