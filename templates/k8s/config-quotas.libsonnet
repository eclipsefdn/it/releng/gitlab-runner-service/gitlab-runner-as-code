// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

{
  gen(config): config {
    local hardctx = {
      [if ctxValue.resourceCalculation then 'hard' + ctxValue.ctx]: {
        pods: 1 + ctxValue.runner.global.concurrent + ctxValue.resources.stalePods,
        cpuRequest: ctxValue.resources.runner.cpuRequest + ctxValue.runner.global.concurrent * (ctxValue.resources.init.cpuRequest + ctxValue.resources.build.cpuRequest + (if ctxValue.servicesActive then ctxValue.resources.service.cpuRequest else 0) + ctxValue.resources.helper.cpuRequest),
        cpuLimit: ctxValue.resources.runner.cpuLimit + ctxValue.runner.global.concurrent * (ctxValue.resources.init.cpuLimit + ctxValue.resources.build.cpuLimit + (if ctxValue.servicesActive then ctxValue.resources.service.cpuLimit else 0) + ctxValue.resources.helper.cpuLimit),
        memoryRequest: ctxValue.resources.runner.memoryRequest + ctxValue.runner.global.concurrent * (ctxValue.resources.init.memoryRequest + ctxValue.resources.build.memoryRequest + (if ctxValue.servicesActive then ctxValue.resources.service.memoryRequest else 0) + ctxValue.resources.helper.memoryRequest),
        memoryLimit: ctxValue.resources.runner.memoryLimit + ctxValue.runner.global.concurrent * (ctxValue.resources.init.memoryLimit + ctxValue.resources.build.memoryLimit + (if ctxValue.servicesActive then ctxValue.resources.service.memoryLimit else 0) + ctxValue.resources.helper.memoryLimit),
           
        runnerCpuRequest: ctxValue.runner.global.concurrent * ctxValue.resources.build.cpuRequest,
        runnerCpuLimit: ctxValue.runner.global.concurrent * ctxValue.resources.build.cpuLimit,
        runnerMemoryRequest: ctxValue.runner.global.concurrent * ctxValue.resources.build.memoryRequest,
        runnerMemoryLimit: ctxValue.runner.global.concurrent * ctxValue.resources.build.memoryLimit,

        serviceCpuRequest: ctxValue.runner.global.concurrent * ctxValue.resources.service.cpuRequest,
        serviceCpuLimit: ctxValue.runner.global.concurrent * ctxValue.resources.service.cpuLimit,
        serviceMemoryRequest: ctxValue.runner.global.concurrent * ctxValue.resources.service.memoryRequest,
        serviceMemoryLimit: ctxValue.runner.global.concurrent * ctxValue.resources.service.memoryLimit,

        helperCpuRequest: ctxValue.runner.global.concurrent * ctxValue.resources.helper.cpuRequest,
        helperCpuLimit: ctxValue.runner.global.concurrent * ctxValue.resources.helper.cpuLimit,
        helperMemoryRequest: ctxValue.runner.global.concurrent * ctxValue.resources.helper.memoryRequest,
        helperMemoryLimit: ctxValue.runner.global.concurrent * ctxValue.resources.helper.memoryLimit,    
      }
      for ctxValue in std.objectValues(config.deploymentctx)
    },
    //debug: hardctx,
    local accumulateValues(propName) =
      std.foldl(function(x, y) (x + y), std.map(function(x) x[propName], std.objectValues(hardctx)), 0),

    project+: { resourcesQuotas+: {
      pods: accumulateValues('pods'),
      cpuRequest: accumulateValues('cpuRequest'),
      cpuLimit: accumulateValues('cpuLimit'),
      memoryRequest: accumulateValues('memoryRequest'),
      memoryLimit: accumulateValues('memoryLimit'),
    } },

    deploymentctx+: {
      [ctx]:
        std.mergePatch(
          config.deploymentctx[ctx],
          { runner+: { env+: { kubernetes+: {
            cpuRequestOverwriteMaxAllowed: '%dm' % accumulateValues('runnerCpuRequest'),
            cpuLimitOverwriteMaxAllowed: '%dm' % accumulateValues('runnerCpuLimit'),
            memoryRequestOverwriteMaxAllowed: '%dMi' % accumulateValues('runnerMemoryRequest'),
            memoryLimitOverwriteMaxAllowed: '%dMi' % accumulateValues('runnerMemoryLimit'),

            serviceCpuRequestOverwriteMaxAllowed: '%dm' % accumulateValues('serviceCpuRequest'),
            serviceCpuLimitOverwriteMaxAllowed: '%dm' % accumulateValues('serviceCpuLimit'),
            serviceMemoryRequestOverwriteMaxAllowed: '%dMi' % accumulateValues('serviceMemoryRequest'),
            serviceMemoryLimitOverwriteMaxAllowed: '%dMi' % accumulateValues('serviceMemoryLimit'),

            helperCpuRequestOverwriteMaxAllowed: '%dm' % accumulateValues('helperCpuRequest'),
            helperCpuLimitOverwriteMaxAllowed: '%dm' % accumulateValues('helperCpuLimit'),
            helperMemoryRequestOverwriteMaxAllowed: '%dMi' % accumulateValues('helperMemoryRequest'),
            helperMemoryLimitOverwriteMaxAllowed: '%dMi' % accumulateValues('helperMemoryLimit'),
          } } } }
        )
      for ctx in std.objectFields(config.deploymentctx)
    },
  },
}
