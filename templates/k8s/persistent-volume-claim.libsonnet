// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import "kube.libsonnet";
{
  gen_pv(config, definition, ctx): Kube.PersistentVolume(config.project.kubeNamespaceName, config, ctx) {
    spec: 
      std.mergePatch(
        definition,
        {
          claimRef+: {
            name: if ctx != '' then config.project.kubeNamespaceName + '-' + ctx + '-' + Kube.namingPersistentVolumeClaim else config.project.kubeNamespaceName + '-' + Kube.namingPersistentVolumeClaim,
          }, 
        }
    ),
  },

  gen_pvc(config, definition, ctx): Kube.PersistentVolumeClaim(config.project.kubeNamespaceName, config, ctx) {
    spec: definition,
  },
}

