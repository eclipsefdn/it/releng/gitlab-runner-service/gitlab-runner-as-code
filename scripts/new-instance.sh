#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

DRY_MESSAGE=""
: "${GRAC_DRY:=""}"
[[ -n "${GRAC_DRY}" ]] && [[ "${GRAC_DRY}" == "true" ]] && DRY_MESSAGE="Dry mode (ON)"

IFS=$'\n\t'
script_name="$(basename "${BASH_SOURCE[0]}")"
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/log.sh"

log info  "############################ START new_instance ${DRY_MESSAGE} ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

project_name="${1:-}"
project_namespace_alias="${2:-}"

start=$(date +"%s")

usage() {
  log info "Usage: %s project_name \n" "$script_name" "\n" \
    "\t%-16s full name (e.g. ./instances/eclipsefdn.it.releng.gitlab-runner-as-code for GRAC project).\n" "project_name"
}

# check that project name is not empty
if [[ -z "${project_name}" ]]; then
  log error "A project name must be given.\n"
  usage
  exit 1
fi


# check that project name contains a dot
if [[ "${project_name}" != *.* ]]; then
  log error "The full project name with a dot must be given (e.g. eclipsefdn.it.releng.gitlab-runner-as-code).\n actual project name:" "$project_name"
  usage
  exit 1
fi

transform_instance() {
  local instance_name="${1:-}"
  # check that parameter is not empty
  if [[ -z "${instance_name}" ]]; then
    printf "ERROR: a %s must be given.\n" "${instance_name}"
    exit 1
  fi
  instance_repo=${instance_name##*/}
  echo "${instance_repo}"
}

project_short_code() {
  local instance_name="${1:-}"
  # check that parameter is not empty
  if [[ -z "${instance_name}" ]]; then
    printf "ERROR: a %s must be given.\n" "${instance_name}"
    exit 1
  fi
  instance_repo=${instance_name##*/}
  echo "${instance_repo//./\/}"
}

fullname_shortcut(){
  local fullName="${1:-}"

  fullNameParts=($(echo $fullName | tr "." "\n"))

  fullNameCut=""

  for (( i=0; i<"${#fullNameParts[@]}"; i++ ));
  do
      fullNamePart=${fullNameParts[$i]}
      dastParts=($(echo "${fullNamePart}" | tr "-" "\n"))
      for (( j=0; j<"${#dastParts[@]}"; j++ ));
      do
          dastPart=${dastParts[$j]}
          fullNameCut+=${dastPart:0:3}
          [[ $j -lt ${#dastParts[@]}-1 ]] && fullNameCut+="-"
      done
      [[ $i -lt ${#fullNameParts[@]}-1 ]] && fullNameCut+="."
  done
  echo "${fullNameCut}"
}

instance_repo="$(transform_instance "${project_name}")"

if [[ -z $project_namespace_alias ]]; then
  instance_repo_gitlab="$(project_short_code "${project_name}")"

  # kubernetes label name 63 character limit
  if [[ ${#instance_repo} -gt 63 ]]; then
    instance_repo_shortcut="$(fullname_shortcut "${instance_repo}")"
    log warn "Project name is too long for kubernetes (>63 char)? (Proposition: ${instance_repo_shortcut})"
    instance_repo="${instance_repo_shortcut}"
  fi
else
  instance_repo_gitlab=${project_namespace_alias}
fi

if [[ -z "${GRAC_DRY}" ]] || [[ "${GRAC_DRY}" == "false" ]]; then
  project_group=$("${SCRIPT_FOLDER}"/gitlab_groups_projects.sh get_repo_type "${instance_repo_gitlab}")
  log info "You are about to create instance for ${project_group} ${instance_repo_gitlab}"
else
  log warn "Dry Mode: On, group or project check is deactivated!"
fi 

mkdir -p "${SCRIPT_FOLDER}/../instances/${instance_repo}/${CURRENT_CLUSTER_CONTEXT}"

cat <<EOG > "${SCRIPT_FOLDER}/../instances/${instance_repo}/${CURRENT_CLUSTER_CONTEXT}/grac.jsonnet"
local grac = import '../../../templates/grac.libsonnet';

grac+ {
  "config.json"+: {
    project+: {
      fullName: '${instance_repo}',
    },
    deployment+: {
      cluster: '${CURRENT_CLUSTER_CONTEXT}',
      namespace: '${instance_repo_gitlab}',
    },
  },
}
EOG

end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END new_instance in ${runtime}s ############################"