// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';

{
  gen(config): Kube.ResourceQuota(config.project.kubeNamespaceName, config) {
    spec: {      
      hard: {
        pods: config.project.resourcesQuotas.pods,
        'requests.cpu': '%dm' % config.project.resourcesQuotas.cpuRequest,
        'requests.memory': '%dMi' % config.project.resourcesQuotas.memoryRequest,
        'limits.cpu': '%dm' % config.project.resourcesQuotas.cpuLimit,
        'limits.memory': '%dMi' % config.project.resourcesQuotas.memoryLimit,
      },
    },
  },
}
