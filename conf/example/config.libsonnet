// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0
local Const = std.extVar('packs');
local Small = std.extVar('small');
local Medium = std.extVar('medium');
local Large = std.extVar('large');
{
  'config.json'+: {},
    
} + 
{
  project+: {
      resourcePacks: 1,
      limitRange: true,
      resourcesQuotas:{
        active: true,
        auto: true,
      },
  },
  deploymentctx+:{
      [if $.project.resourcePacksActive then "small"]: {
        runnerType: 'groups',
        resources+:{
          build: {
            cpuRequest: Small.build_cpu_request,
            cpuLimit: Small.build_cpu_limit,
            memoryRequest: Small.build_mem_request,
            memoryLimit: Small.build_mem_limit,
            storageRequest: Small.build_storage_request,
            storageLimit: Small.build_storage_limit,
          },
        },
        runner+:{
          global+: {
            concurrent: Small.build_concurrent + (Small.build_concurrent_increment_per_pack * ($.project.resourcePacks - 1))
          }
        }
      },
      [if $.project.resourcePacksActive then "medium"]: {
        runnerType: 'groups',
        resources+:{
          build: {
            cpuRequest: Medium.build_cpu_request,
            cpuLimit: Medium.build_cpu_limit,
            memoryRequest: Medium.build_mem_request,
            memoryLimit: Medium.build_mem_limit,
            storageRequest: Medium.build_storage_request,
            storageLimit: Medium.build_storage_limit,
          },
        },
        runner+:{
          global+: {
            concurrent: Medium.build_concurrent + (Medium.build_concurrent_increment_per_pack * ($.project.resourcePacks - 1))
          }
        }
      },
      [ if $.project.resourcePacksActive && $.project.resourcePacks > 2 then "large"]: {
        runnerType: 'groups',
        resources+:{
          build: {
            cpuRequest: Large.build_cpu_request,
            cpuLimit: Large.build_cpu_limit,
            memoryRequest: Large.build_mem_request,
            memoryLimit: Large.build_mem_limit,
            storageRequest: Large.build_storage_request,
            storageLimit: Large.build_storage_limit,
          },
        },
        runner+:{
          global+: {
            concurrent: std.floor(Large.build_concurrent + (Large.build_concurrent_increment_per_pack * $.project.resourcePacks)) 
          }
        }
      },
  }
}