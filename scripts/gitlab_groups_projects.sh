#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# GitLab admin functions

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

SCRIPT_FOLDER="$(dirname "$(readlink -f "${0}")")"

# shellcheck disable=SC1091
source "${SCRIPT_FOLDER}/gitlab_context.sh"

help() {
  printf "Available commands:\n"
  printf "Command\t\t\tDescription\n\n"
  printf "is_typeof\t\tIs repo of type.\n"
  printf "get_repo_type\t\tGet repo type project or group.\n"
  exit 0
}

is_typeof() {
  local repo_name="${1:-}"
  local repo_type="${2:-}"

  check_parameter "repo name" "${repo_name}"

  local repo_name_encode
  repo_name_encode="$(urlencode "${repo_name}")"
  
  status_code=$(curl --write-out "%{http_code}\n" --output /dev/null -sSL --header "${TOKEN_HEADER}" "${API_BASE_URL}/${repo_type}/${repo_name_encode}")
  
  if [[ "$status_code" -ne 200 ]]; then
    log debug "status_code: $status_code"
    exit 1
  else
    exit 0
  fi
}

#######################################
# Get repo type of a repo name
# ARGUMENTS:
#   repo_name: project or group as describe with path_with_namespace in projects api or full_path in groups api 
# RETURN:
#   projects or groups
#######################################
get_repo_type() {
  local repo_name="${1:-}"

  check_parameter "repo name" "${repo_name}"

  local repo_type=""
  local repo_type_projects="projects"
  local repo_type_groups="groups"
  local repo_type_shared="shared"

  #[[ $(is_project "${repo_name}" && echo $?) -eq 0 ]] && repo_type="${repo_type_projects}"
  # shellcheck disable=SC2091
  $(is_typeof "${repo_name}" "projects") && repo_type="${repo_type_projects}"
  # shellcheck disable=SC2091
  [[ -z "${repo_type}" ]] && $(is_typeof "${repo_name}" "groups") && repo_type="${repo_type_groups}"

  # shared runner management
  [[ $repo_name == *"${repo_type_shared}" ]] && repo_type="${repo_type_shared}"
  [[ -z "${repo_type}" ]] && log error "No gitlab project or group find for ${GITLAB_URL}/${repo_name}" && exit 1

  echo "$repo_type"
}

get_repo_id() {
  local repo_name="${1:-}"
  local type="${2:-}"

  local http_code
  local content

  check_parameter "repo name" "${repo_name}"

  local repo_name_encode
  repo_name_encode="$(urlencode "${repo_name}")"

  [[ -z "$type" ]] && type=$(get_repo_type "${repo_name}") 

  response=$(curl -sSL --header "${TOKEN_HEADER}" -w "\n%{http_code}" "${API_BASE_URL}/${type}/${repo_name_encode}")

  http_code=$(echo "$response" | tail -n 1)

  # Extract content from the response variable (excluding the last line with the HTTP code)
  content=$(echo "$response" | head -n -1)

  # Check the HTTP response code and handle accordingly
  if [ "$http_code" -eq 200 ]; then
      echo "$content"| jq -r '.id'
  else
      echo "Request failed for ${repo_name}. HTTP code: $http_code"
  fi
}

"$@"

# show help menu, if no first parameter is given
if [[ -z "${1:-}" ]]; then
  help
fi