#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"


# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"

log info  "############################ START K8S_RESTART ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instance="${1:-}"
deployment="${2:-}"

start=$(date +"%s")

if [ -z "${instance}" ]; then
  log error "You must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log error "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 1
fi

if [[ -z "${deployment}" ]]; then
  CONFIG_JSON="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/config.json"
  allDeploymentTypes="$(jq -r '.project.deployment | to_entries' "${CONFIG_JSON}" | jq -r '.[].value')"
  for deploymentType in ${allDeploymentTypes} 
  do
    "${SCRIPT_FOLDER}/k8s-restart.sh" "${instance}" "${deploymentType}" || :
  done
else
  deploymentName="$(jq -r '.metadata.name' "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/statefulset-${deployment}.json")"
  ns="$(jq -r '.metadata.namespace' "${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s/statefulset-${deployment}.json")"

  kubectl rollout restart -n "${ns}" statefulset "${deploymentName}"
  kubectl rollout status -n "${ns}" statefulset "${deploymentName}"
fi

end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END K8S_RESTART in ${runtime}s ############################"