// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local config = import './config.json';
local deploymentctx = std.extVar('deploymentctx');

{}
+ 
{
  'k8s/namespace.json': (import 'k8s/namespace.libsonnet').gen(config),
  'k8s/role.json': (import 'k8s/role.libsonnet').gen(config),
  'k8s/role-binding.json': (import 'k8s/role-binding.libsonnet').gen(config),
  'k8s/service-account.json': (import 'k8s/service-account.libsonnet').gen(config),
 
  [if (config.project.resourcesQuotas.active) then 
    'k8s/resource-quotas.json']: (import 'k8s/resource-quotas.libsonnet').gen(config),
  [if (config.project.limitRange) then 
    'k8s/limit-range.json']: (import 'k8s/limit-range.libsonnet').gen(config),


  [if (std.objectHas(config.deployment.runner.runners.kubernetes, 'sessionServer')) then 
    'k8s/service-session-server.json']: (import 'k8s/service-session-server.libsonnet').gen(config),

  [if (config.deployment.persistentVolumeClaim.active && config.deployment.persistentVolumeClaim.isGlobalDefinition) then 
    'k8s/persistent-volume-claim.json']: (import 'k8s/persistent-volume-claim.libsonnet').gen_pvc(config, config.deployment.persistentVolumeClaim.definition, ''),
  [if (config.deployment.persistentVolumeClaim.active && config.deployment.persistentVolumeClaim.isGlobalDefinition && config.deployment.persistentVolumeClaim.persistentVolume.active) then 
    'k8s/persistent-volume.json']: (import 'k8s/persistent-volume-claim.libsonnet').gen_pv(config, config.deployment.persistentVolumeClaim.persistentVolume.definition, ''),
}
+
{
  [if (std.get(config.deploymentctx, deploymentctx).active && std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.active && !std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.isGlobalDefinition) then 
    'k8s/persistent-volume-claim-' + deploymentctx + '.json']: (import 'k8s/persistent-volume-claim.libsonnet').gen_pvc(config, std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.definition, deploymentctx),
  for deploymentctx in std.objectFields(config.deploymentctx)
} 
+
{   
  [if (std.get(config.deploymentctx, deploymentctx).active && std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.active && !std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.isGlobalDefinition && std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.persistentVolume.active) then 
    'k8s/persistent-volume-' + deploymentctx + '.json']: (import 'k8s/persistent-volume-claim.libsonnet').gen_pv(config, std.get(config.deploymentctx, deploymentctx).persistentVolumeClaim.persistentVolume.definition, deploymentctx),
  for deploymentctx in std.objectFields(config.deploymentctx)
}+
{
  [if config.project.serviceMetrics && std.get(config.deploymentctx, deploymentctx).active then 
    'k8s/service-metrics-' + deploymentctx + '.json']: (import 'k8s/service-metrics.libsonnet').gen(config, std.get(config.deploymentctx, deploymentctx), deploymentctx)
  for deploymentctx in std.objectFields(config.deploymentctx)
} 
+
{
  [if std.get(config.deploymentctx, deploymentctx).active then 'k8s/statefulset-' + deploymentctx + '.json']: (import 'k8s/statefulset.libsonnet').gen(config, std.get(config.deploymentctx, deploymentctx), deploymentctx)
  for deploymentctx in std.objectFields(config.deploymentctx)
} 
