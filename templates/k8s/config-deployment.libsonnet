// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

{
  // TODO replace all config.deployment with config.deploymentctx, in tag list after merge user config.

  gen(config): config + {
    local configctx = std.mergePatch(
      {
        [ctx]: config.deployment,
        for ctx in std.objectFields(config.deploymentctx)
      },
      if(std.objectHas(config, 'deploymentctx')) then config.deploymentctx else {},
    ),
    deploymentctx+: {          
      local ctxValue = configctx[ctx],
        [ctx]: std.mergePatch(
          ctxValue,
          {
            ctx: ctx,
            namespace: if std.objectHas(ctxValue, 'namespace') && ctxValue.namespace != "" then ctxValue.namespace else error 'Runner: deploymentctx.namespace is missing for ' + ctx + ', please define gitlab namespace in deployment context"',
            runner:{
              env:{
                runner: {
                  local getTagValue(key, value, limiter=',') = 
                    if value != '' && value != {} then (key + ':' + value + limiter) else '',
                  local getDefaultTagList() =
                    getTagValue('prefix', config.project.prefix) + 
                    getTagValue('cluster', ctxValue.cluster) + 
                    getTagValue('type', if std.objectHas(ctxValue, 'runnerType') then ctxValue.runnerType else 'groups') + 
                    getTagValue('instance', config.project.fullName) + 
                    getTagValue('ctx', ctx) + 
                    getTagValue('executor', ctxValue.runner.env.runner.executor, ''),
                  local getMoreInfoTagList() =
                    getTagValue('kubenamespace', ctxValue.runner.env.kubernetes.namespace) + 
                    if std.objectHas(ctxValue.runner.runners.kubernetes, 'nodeSelector') then getTagValue('nodeSelector', std.mergePatch(ctxValue.runner.env.kubernetes.nodeSelector,ctxValue.runner.runners.kubernetes.nodeSelector)) else '' + // TODO transform as a list
                    if std.objectHas(ctxValue.runner.runners.kubernetes, 'nodeTolerations') then getTagValue('nodeTolerations', std.mergePatch(ctxValue.runner.env.kubernetes.nodeTolerations,ctxValue.runner.runners.kubernetes.nodeTolerations)) else '' + // TODO transform as a list
                    getTagValue('concurrent', ctxValue.runner.global.concurrent) + 
                    getTagValue('outputLimit', ctxValue.runner.env.runner.outputLimit) + 
                    getTagValue('image', ctxValue.runner.env.kubernetes.image) + 
                    getTagValue('cpuLimit', ctxValue.resources.build.cpuLimit) + 
                    getTagValue('cpuRequest', ctxValue.resources.build.cpuRequest) + 
                    getTagValue('memoryLimit', ctxValue.resources.build.memoryLimit) + 
                    getTagValue('memoryRequest', ctxValue.resources.build.memoryRequest) + 
                    getTagValue('persistentVolumeClaim', ctxValue.persistentVolumeClaim.active) +
                    getTagValue('persistentVolume', ctxValue.persistentVolumeClaim.persistentVolume.active)+
                    getTagValue('secretsmanager', config.project.secretsManager.active) +
                    if ctxValue.servicesActive then getTagValue('serviceCpuLimit', ctxValue.resources.service.cpuLimit) else ''+ 
                    if ctxValue.servicesActive then getTagValue('serviceCpuRequest', ctxValue.resources.service.cpuRequest) else ''+ 
                    if ctxValue.servicesActive then getTagValue('serviceMemoryLimit', ctxValue.resources.service.memoryLimit) else ''+ 
                    if ctxValue.servicesActive then getTagValue('serviceMemoryRequest', ctxValue.resources.service.memoryRequest) else '',                  gracDefaultTagList: getDefaultTagList(), // use to find and delete runner
                  tagList: getDefaultTagList() + ',' + getMoreInfoTagList() + ',' + ctxValue.runner.env.runner.tagList,   # Tag list [$RUNNER_TAG_LIST]
                  name: config.project.prefix + '-' + config.project.fullName + '-' + ctx + '-runner',
                },
                kubernetes+: {
                  local getValue(value, units) = 
                    if value != '' && value != 0 then units % value else '',
                  cpuLimit: getValue(ctxValue.resources.build.cpuLimit, '%dm'),                     # The CPU allocation given to build containers [$KUBERNETES_CPU_LIMIT]
                  cpuRequest: getValue(ctxValue.resources.build.cpuRequest, '%dm'),                 # The CPU allocation requested for build containers [$KUBERNETES_CPU_REQUEST]
                  memoryLimit: getValue(ctxValue.resources.build.memoryLimit, '%dMi'),               # The amount of memory allocated to build containers [$KUBERNETES_MEMORY_LIMIT]
                  memoryRequest: getValue(ctxValue.resources.build.memoryRequest, '%dMi'),           # The amount of memory requested from build containers [$KUBERNETES_MEMORY_REQUEST]
                  ephemeralStorageLimit: getValue(ctxValue.resources.build.storageLimit, '%dMi'),    # The amount of ephemeral storage allocated to build containers [$KUBERNETES_EPHEMERAL_STORAGE_LIMIT]
                  ephemeralStorageRequest: getValue(ctxValue.resources.build.storageRequest, '%dMi'),# The amount of ephemeral storage requested from build containers [$KUBERNETES_EPHEMERAL_STORAGE_REQUEST]
                  serviceCpuLimit: if ctxValue.servicesActive then getValue(ctxValue.resources.service.cpuLimit, '%dm') else '',            # The CPU allocation given to build service containers [$KUBERNETES_SERVICE_CPU_LIMIT]
                  serviceCpuRequest: if ctxValue.servicesActive then getValue(ctxValue.resources.service.cpuRequest, '%dm') else '',        # The CPU allocation requested for build service containers [$KUBERNETES_SERVICE_CPU_REQUEST]
                  serviceMemoryLimit: if ctxValue.servicesActive then getValue(ctxValue.resources.service.memoryLimit, '%dMi') else '',      # The amount of memory allocated to build service containers [$KUBERNETES_SERVICE_MEMORY_LIMIT]
                  serviceMemoryRequest: if ctxValue.servicesActive then getValue(ctxValue.resources.service.memoryRequest, '%dMi') else '',  # The amount of memory requested for build service containers [$KUBERNETES_SERVICE_MEMORY_REQUEST]
                  serviceEphemeralStorageLimit: if ctxValue.servicesActive then getValue(ctxValue.resources.service.storageLimit, '%dMi') else '', # The amount of ephemeral storage allocated to build service containers [$KUBERNETES_SERVICE_EPHEMERAL_STORAGE_LIMIT]
                  serviceEphemeralStorageRequest: if ctxValue.servicesActive then getValue(ctxValue.resources.service.storageRequest, '%dMi') else '', # The amount of ephemeral storage requested for build service containers [$KUBERNETES_SERVICE_EPHEMERAL_STORAGE_REQUEST]
                  helperCpuLimit: getValue(ctxValue.resources.helper.cpuLimit, '%dm'),              # The CPU allocation given to build helper containers [$KUBERNETES_HELPER_CPU_LIMIT]
                  helperCpuRequest: getValue(ctxValue.resources.helper.cpuRequest, '%dm'),          # The CPU allocation requested for build helper containers [$KUBERNETES_HELPER_CPU_REQUEST]
                  helperMemoryLimit: getValue(ctxValue.resources.helper.memoryLimit, '%dMi'),        # The amount of memory allocated to build helper containers [$KUBERNETES_HELPER_MEMORY_LIMIT]
                  helperMemoryRequest: getValue(ctxValue.resources.helper.memoryRequest, '%dMi'),    # The amount of memory requested for build helper containers [$KUBERNETES_HELPER_MEMORY_REQUEST]
                  helperEphemeralStorageLimit: getValue(ctxValue.resources.helper.storageLimit, '%dMi'), # The amount of ephemeral storage allocated to build helper containers [$KUBERNETES_HELPER_EPHEMERAL_STORAGE_LIMIT]
                  helperEphemeralStorageRequest: getValue(ctxValue.resources.helper.storageRequest,'%dMi'), # The amount of ephemeral storage requested for build helper containers [$KUBERNETES_HELPER_EPHEMERAL_STORAGE_REQUEST]                 
                }
              }
            }
          }
        ),
        for ctx in std.objectFields(configctx)
      },
  },
}

