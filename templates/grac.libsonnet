// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local externalConfig = std.extVar('config');

{}
+
{
  'config.json': import 'config.libsonnet'
}
+
{
  'config.json'+: externalConfig,
}
