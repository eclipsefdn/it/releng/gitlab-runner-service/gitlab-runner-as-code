// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0
{

  // runner
  runner_cpu_request::100,
  runner_cpu_limit::200,
  runner_mem_request::128,
  runner_mem_limit::256,

  # helper
  helper_cpu_request::100,
  helper_cpu_limit::150,
  helper_mem_request::128,
  helper_mem_limit::128,

  helper_storage_request::100,
  helper_storage_limit::100,

  # init perm
  init_cpu_request::100,
  init_cpu_limit::150,
  init_mem_request::128,
  init_mem_limit::128,

  # build 
  build_cpu_request::250,
  build_cpu_limit::500,
  build_mem_request::1024,
  build_mem_limit::1024,

  build_storage_limit::20*1000,
  build_storage_request::10*1000,

  build_concurrent::3,

  # service
  service_cpu_request::500,
  service_cpu_limit::1000,
  service_mem_request::1*1024,
  service_mem_limit::2*1024,
  
  service_storage_request::100,
  service_storage_limit::200,
  
  # limit range
  limit_pod_concurrent::2,
  limit_pod_min_cpu::100,
  limit_pod_min_mem::8,
  limit_container_min_cpu::100,
  limit_container_min_mem::8,
  
} 
  
 
  