
<!--
SPDX-FileCopyrightText: 2024 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# Administration

## Force Delete a Kubernetes Namespace

```shell 
NAMESPACE=grac-eclipsefdn-it-releng-gitlab-runner-as-code
kubectl get namespace $NAMESPACE -o json|sed 's/"kubernetes"//'  > /tmp/tmp.json 

```

Delete finalizers `kubernetes` from namespace definition.

```json
    "spec": {
        "finalizers": [
        ]
    },
```

Apply modification : 

```shell 
kubectl proxy
curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp.json http://127.0.0.1:8001/api/v1/namespaces/$NAMESPACE/finalize
```

## Force Delete pod

Pod may stay in terminated mode. Only a force delete in this case will force termination.


1. Get pod name

```shell
kubectl get pods -n ef-grac-technology-cbi
```

2. Force deletion

```shell
kubectl delete pod cbi-grac-default-sfs-0 --force -n ef-grac-technology-cbi
```

## clean up pod

https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/-/blob/main/readme.md