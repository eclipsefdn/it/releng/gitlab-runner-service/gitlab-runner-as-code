#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# * Call gen-yaml.sh script

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"

log info  "############################ START k8s ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

templates="${SCRIPT_FOLDER}/../templates"

instance="${1:-}"
deployment="${2:-}"

start=$(date +"%s")

if [ -z "${instance}" ]; then
  log error "you must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log warn "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 
fi

CONFIG_JSON="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/config.json"

if [[ ! -f "${CONFIG_JSON}" ]]; then
  log error "No config.json found at '${CONFIG_JSON}', please run first './grac.sh k8s ${instance}'"
  exit 1
fi

TARGET="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/k8s"
mkdir -p "${TARGET}"
rm -rf "${TARGET:?}/*"

generateTemplate() {
  local deploymentType=$1

  deploymentctx="$(jq  -r --arg deploymentctx "${deploymentType}" '.deploymentctx | .[$deploymentctx]' "${CONFIG_JSON}")"
  active="$(echo "${deploymentctx}" | jq -r '.active == true')"
  [[ "${active}" == true ]] && echo "Generate template deployment \"${deploymentType}\": ${TARGET}/configmap-${deploymentType}.yaml" && 
    hbs -s -H "${templates}"'/k8s/helpers/*.js' -D "${CONFIG_JSON}" -D "${deploymentctx}" -P "${templates}"'/k8s/partials/*.hbs' -P "${instance}/${CURRENT_CLUSTER_CONTEXT}"'/target/grac/*.yaml' "${templates}/k8s/configmap.yaml.hbs" > "${TARGET}/configmap-${deploymentType}.yaml"
}

# activate module debug from hbs-cli
#export DEBUG="*"
allDeploymentTypes="$(jq -r '.deploymentctx | to_entries' "${CONFIG_JSON}" | jq -r '.[].key')"
for deploymentType in ${allDeploymentTypes} 
do
  if [[ -n "${deployment}" ]]; then
    if [[ "${deployment}" == "${deploymentType}" ]]; then
        generateTemplate "${deploymentType}"
      break
    fi
  else
    generateTemplate "${deploymentType}"
  fi 
done

end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END k8s in ${runtime}s ############################"