#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

set -o errexit
set -o nounset
set -o pipefail

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/scripts/log.sh"

usage() {
  cat << EOF # remove the space between << and EOF, this is due to web plugin issue
Usage: $(basename "${BASH_SOURCE[0]}") (create|init|genconfig|k8s|deploy|restart|replay|reload|delete|...) <instance name> [-h] [-v] [-a] [-m] [-t] [-d] [-s] [-p] [-i]

Available options:

-h  Help
-v  Verbose mode

# GRAC! params: 
-a  Alias
-n  Deployment Name
-g  Gitlab URL 
-p  Gitlab TOKEN 
-d  Debug mode 
-r  Dry mode 

# GRAC! examples: 

./grac.sh create oniro.oniro-core
./grac.sh create foundation-internal.infra -a eclipsefdn/it/internal/infra
./grac.sh deploy oniro.oniro-core
./grac.sh deploy oniro.oniro-core -n medium
./grac.sh deploy foundation-internal.infra -r true
./grac.sh reload oniro.oniro-core
./grac.sh reload technology.cbi -n grac-fastdraft
./grac.sh create research.spade -a eclipse-research-labs/spade-project

EOF
  exit
}

action="${1:-}"
instance="${2:-}"

gitlab="https://gitlab.eclipse.org"
alias=""
deployment=""
pat=""
debug=""
dry=""

shift 2

while getopts ":hva:n:g:p:d:r:" option; do
  case $option in
  h) usage;;
  v) set -x;; 
  a)
    alias="${OPTARG:-}"
    ;;
  n)
    deployment="${OPTARG:-}"
    ;;
  g)
    gitlab="${OPTARG:-"${gitlab}"}"
    ;;
  p)
    pat="${OPTARG:-}"
    ;;
  d)
    debug="${OPTARG:-}"
    ;;
  r)
    dry="${OPTARG:-}"
    ;;
  :)
    echo "the option -$OPTARG need an argument." >&2
    exit 1
    ;;
  -?*) echo "Unknown option: $1" && exit 1 ;;
  *) break ;;
  esac
done

cat <<EOF
GRAC Options:
  Action:      ${action}
  Instance:    ${instance}
  GitLab:      ${gitlab}
  Alias Name:  ${alias}
  Deployment:  ${deployment}
  PAT:         ${pat}
  Debug:       ${debug}
  Dry Run:     ${dry}
EOF

if [ -z "${action}" ]; then
  log error "You must provide an 'instance' name argument"
  exit 1
fi

if [[ -z "${instance}" ]] && [[ "${action}" != "context" ]] && [[ "${action}" != *"-all" ]]; then
  log error "You must provide an 'instance' name argument"
  exit 1
fi

ENV_GITLAB=(--env GITLAB_URL="${gitlab}")

if [[ ${action} == "create" ]] && [[ -z ${alias} ]]; then

  if [[ ${instance} != "foundation-internal"* ]] && [[ ${instance} != "user"* ]]; then
    alias="eclipse/${instance##*.}"
  else
    log error "Please specify a gitlab namespace, e.g: ./grac.sh create foundation-internal.infra eclipsefdn/it/internal/infra"
    exit 1
  fi
fi

ENV_PERSONAL_ACCESS_TOKEN=(--env PERSONAL_ACCESS_TOKEN="")
[[ -n "${pat}" ]] && ENV_PERSONAL_ACCESS_TOKEN=(--env PERSONAL_ACCESS_TOKEN="${pat}")

ENV_DEBUG=(--env GRAC_DEBUG="false")
[[ -n "${debug}" ]] && ENV_DEBUG=(--env GRAC_DEBUG="true")

ENV_DRY=(--env GRAC_DRY="false")
[[ -n "${dry}" ]] && ENV_DRY=(--env GRAC_DRY="true")

log info  "############################ START GRAC! on ${gitlab} ############################"
docker run \
    --user grac \
    -v "${PWD}/instances":/home/grac/instances:rw \
    -v "${PWD}/conf":/home/grac/conf:ro \
    -v ~/.cbi/config:/home/grac/.cbi/config:ro \
    -v ~/.kube/config:/home/grac/.kube/config:ro \
    -v /etc/timezone:/etc/timezone:ro \
    -v /etc/localtime:/etc/localtime:ro \
    --env KUBECONFIG=/home/grac/.kube/config \
    "${ENV_GITLAB[@]}" \
    "${ENV_PERSONAL_ACCESS_TOKEN[@]}" \
    "${ENV_DEBUG[@]}" \
    "${ENV_DRY[@]}" \
    --network host \
    eclipsecbi/grac make "${action}" instance="${instance}" alias="${alias}" deployment="${deployment}"

log info  "############################ END GRAC! ############################"
