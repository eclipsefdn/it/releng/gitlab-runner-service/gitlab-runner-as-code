// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
{
  gen(config, ctx): 
    Kube.Secret(config.project.kubeNamespaceName, config, ctx){
    'stringData': {
      // 'runner-registration-token': "", # need to leave as an empty string for compatibility reasons
      // 'runner-token': std.extVar('runnerToken'),
      'runner-registration-token': std.extVar('runnerToken'), 
      'runner-token': "", # https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/-/issues/201
    },
  },
}