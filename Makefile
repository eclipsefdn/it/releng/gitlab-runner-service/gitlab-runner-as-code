#*******************************************************************************
# Copyright (c) 2024 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the Eclipse Public License 2.0
# which is available at http://www.eclipse.org/legal/epl-v20.html,
# or the MIT License which is available at https://opensource.org/licenses/MIT.
# SPDX-License-Identifier: EPL-2.0 OR MIT
#*******************************************************************************
SHELL=/usr/bin/env bash

INSTANCES_DIR=./instances

create : 
	./scripts/new-instance.sh ${INSTANCES_DIR}/$(instance) ${alias}

context :
	./scripts/k8s-context.sh

registration : delete-runner
	./scripts/gen-registration.sh ${INSTANCES_DIR}/$(instance) ${deployment}

registration-all: delete-all-runner
	./scripts/all.sh gen-registration instances

config :
	./scripts/gen-config.sh ${INSTANCES_DIR}/$(instance) 

config-all: 
	./scripts/all.sh gen-config instances

genk8s : 
	./scripts/gen-k8s.sh ${INSTANCES_DIR}/$(instance) ${deployment}

k8s : clean config 
	./scripts/gen-k8s.sh ${INSTANCES_DIR}/$(instance) ${deployment}

k8s-all : clean-all config-all 
	./scripts/all.sh gen-k8s instances

deploy : registration
	./scripts/k8s-deploy.sh ${INSTANCES_DIR}/$(instance) ${deployment}

deploy-all : registration-all
	./scripts/all.sh k8s-deploy instances

restart :
	./scripts/k8s-restart.sh ${INSTANCES_DIR}/$(instance) ${deployment}

restart-all :
	./scripts/all.sh k8s-restart instances

resume :
	./scripts/resume.sh ${INSTANCES_DIR}/$(instance) ${deployment}

resume-all :
	./scripts/all.sh k8s-resume instances

delete-k8s :
	./scripts/k8s-delete.sh ${INSTANCES_DIR}/$(instance) ${deployment}

delete-runner :
	./scripts/k8s-delete-runner.sh ${INSTANCES_DIR}/$(instance) ${deployment}

delete : delete-runner delete-k8s

delete-all-k8s :
	./scripts/all.sh k8s-delete instances

delete-all-runner :
	./scripts/all.sh k8s-delete-runner instances

delete-all: delete-all-runner delete-all-k8s

replay : delete k8s deploy resume

replay-all : delete-all k8s-all deploy-all resume-all

reload : k8s deploy resume

reload-all : k8s-all deploy-all resume-all

init: reload

clean : 
	./scripts/clean-instance.sh ${INSTANCES_DIR}/$(instance)

clean-all : 
	./scripts/all.sh clean-instance instances

