// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0
{

  build_cpu_request::2000,
  build_cpu_limit::4000,
  build_mem_request::8*1024,
  build_mem_limit::8*1024,

  build_storage_request::10*1000,
  build_storage_limit::20*1000,

  build_concurrent::0,
  build_concurrent_increment_per_pack::0.49,
  
} 
  
 
  