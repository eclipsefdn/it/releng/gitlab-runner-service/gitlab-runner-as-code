<!--
SPDX-FileCopyrightText: 2024 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# Add NFS volume

## Configuration

Activate and configure this Persistence volume in `instances/my_project/okd-cx/config.jsonnet` 

```json

persistentVolume+:{
    active:true,
    declareVolume:true 
},
```

if `declareVolume` is not specify, kubernetes dynamic PV where `persistentVolumeReclaimPolicy` is `delete`.
While if PV is created `persistentVolumeReclaimPolicy` is `retain`.

Allow configuration for each runner : `config.toml` configuration

```json
local Kube = import '../../../templates/k8s/kube.libsonnet';
{
    runner+:{
        kubernetes+:{
            volumes+:{        
                pvc: [
                    {
                        name: $.project.kubeNamespaceName + '-' + Kube.namingPersistentVolumeClaim,
                        mount_path: '/var/shared'
                    },
                ],
            },
        },
    },
}
```

Result in `config.toml`:

```toml
[[runners]]
    [runners.kubernetes]
        [[runners.kubernetes.volumes.pvc]]
          name = "ef-grac-my_project-volume-claim"
          mount_path = "/var/shared"
```


Add right in NFS directory

Default path is specify `instances/my_project/okd-cx/config.jsonnet`  or generate as default in `instances/my_project/okd-cx/target/config.json`  
```json
 "persistentVolume": {
      "path": "/home/data/okd-c1.eclipse.org/PV/my_project",
```

```shell
mkdir -p /home/data/okd-c1.eclipse.org/PV/my_project
chmod 777 /home/data/okd-c1.eclipse.org/PV/my_project
```

## How to control:

In the container

```bash
mount | grep nfs
```

Or with kubectl: 

```bash
kubectl exec -it runner-xxxxxxx-project-xxxx-concurrent-xxxxx -n my_project-namespace -- mount | grep nfs
```