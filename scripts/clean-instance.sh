#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Clean instance

set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"
 
log info  "############################ START clean_instance ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instance="${1:-}"

if [ -z "${instance}" ]; then
  log error "You must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log warn "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 
fi


rm -rf "${instance}/${CURRENT_CLUSTER_CONTEXT}/target"

log info  "############################ END clean_instance ############################"
