// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local config = import './config.json';
local deploymentctx = std.extVar('deploymentctx');

{}
+
{
  [if std.get(config.deploymentctx, deploymentctx).active then'k8s/.secrets/runner-registration-' + deploymentctx + '.json']: (import 'k8s/secret-runner-registration.libsonnet').gen(config, deploymentctx),
} 


