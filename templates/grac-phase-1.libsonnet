// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local config = import './config.json';

{} 
+
{
  'config.json': if (config.project.resourcesQuotas.active && config.project.resourcesQuotas.auto == true) then 
    (import 'k8s/config-quotas.libsonnet').gen((import 'k8s/config-deployment.libsonnet').gen(config)) else
    (import 'k8s/config-deployment.libsonnet').gen(config),
}

