#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# GitLab admin functions

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${0}")")"
LOCAL_CONFIG="${HOME}/.cbi/config"

# shellcheck disable=SC1091
source "${SCRIPT_FOLDER}/log.sh"
# shellcheck disable=SC1091
source "${SCRIPT_FOLDER}/gitlab_utils.sh"

if [[ ! -f "${LOCAL_CONFIG}" ]]; then
  echo "WARN: File '$(readlink -f "${LOCAL_CONFIG}")' does not exists"
  echo "Create one to configure the location of the GitLab token. Example:"
  echo '{"gitlab-token": "SUPER_SECRET_TOKEN"}'
fi

: "${GITLAB_URL:="https://gitlab.com"}"

GITLAB_DOMAIN=$(echo "$GITLAB_URL" | sed -e 's|^[^/]*//||' -e 's|/.*$||')

[[ -z ${PERSONAL_ACCESS_TOKEN:-} ]] && PERSONAL_ACCESS_TOKEN="$(jq -r --arg GITLAB_DOMAIN "${GITLAB_DOMAIN}" '.[$GITLAB_DOMAIN+"-token"]' < "${LOCAL_CONFIG}")"

[[ -n ${PERSONAL_ACCESS_TOKEN} ]] && TOKEN_HEADER="PRIVATE-TOKEN: ${PERSONAL_ACCESS_TOKEN}"
API_BASE_URL="${API_BASE_URL:-"${GITLAB_URL}/api/v4"}"

