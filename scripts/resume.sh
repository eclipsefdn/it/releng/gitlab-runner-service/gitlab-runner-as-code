#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Deploy instance

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"
 
# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/gitlab_context.sh"

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

: "${GRAC_DRY:=""}"
[[ -n "${GRAC_DRY}" ]] && [[ "${GRAC_DRY}" == "true" ]] && \
  log info "Dry Mode: ON, deploy is deactivated!" && exit 0

IFS=$'\n\t'

log info  "############################ START RESUME ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instance="${1:-}"
deployment="${2:-}"

start=$(date +"%s")

if [ -z "${instance}" ]; then
  log error  "You must provide an 'instance' name argument"
  exit 1
fi

if [ ! -d "${instance}/${CURRENT_CLUSTER_CONTEXT}" ]; then
  log error "No 'instance' found at '${instance}/${CURRENT_CLUSTER_CONTEXT}' \n" \
            "Create a new instance with : './grac.sh create ${instance##*/}'" 
  exit 1
fi

CONFIG_JSON="${instance}/${CURRENT_CLUSTER_CONTEXT}/target/config.json"

if [[ ! -f "${CONFIG_JSON}" ]]; then
  log error "No config.json found at '${CONFIG_JSON}', please run first './grac.sh k8s ${instance}'"
  exit 1
fi

resume() {
  local deploymentType="${1:-}"
  deploymentCtxNamespace="$(jq  -r --arg deploymentctx "${deploymentType}" '.deploymentctx | .[$deploymentctx] | .namespace' "${CONFIG_JSON}")"
  tagList="$(jq -r --arg deploymentType "${deploymentType}" '.deploymentctx |  .[$deploymentType] | .runner.env.runner.gracDefaultTagList' "${CONFIG_JSON}")"
  runner_list=$("${SCRIPT_FOLDER}/gitlab_runner_admin.sh" "get_runner_list" "${deploymentCtxNamespace}" "" "${tagList}")
  runnerType="$(jq  -r --arg deploymentctx "${deploymentType}" '.deploymentctx | .[$deploymentctx] | .runnerType' "${CONFIG_JSON}")"
  if [[ ${runnerType} == "groups" ]]; then
    log info "${deploymentType}: ${GITLAB_URL}/groups/${deploymentCtxNamespace}/-/runners/$(echo "${runner_list}" | jq -r '.[].id')"
  else
    log info "${deploymentType}: ${GITLAB_URL}/${deploymentCtxNamespace}/-/runners/$(echo "${runner_list}" | jq -r '.[].id')"
  fi
}

allDeploymentTypes="$(jq -r '.deploymentctx | to_entries' "${CONFIG_JSON}" | jq -r '.[].key')"
for deploymentctx in ${allDeploymentTypes} 
do
  if [[ -n "${deployment}" ]]; then
    if [[ "${deployment}" == "${deploymentctx}" ]]; then
        resume "${deploymentctx}"
      break
    fi
  else
    resume "${deploymentctx}"
  fi 
done

end=$(date +"%s")
runtime=$(echo "$end - $start" | bc -l)
log info  "############################ END RESUME in ${runtime}s ############################"