
<!--
SPDX-FileCopyrightText: 2024 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# runner registration options for kubernetes

TO UPPER CASE:

```shell
docker run gitlab/gitlab-runner:latest register --help| grep kubernetes | awk '{print $1}'| tr '-' '_' |  cut -c 14- | tr a-z A-Z
```

generate configuration CamelCase from helper for kubernetes key word.

```shell
docker run gitlab/gitlab-runner:latest register --help \
     | grep KUBERNETES_ \
     | awk '{print $1}' \
     | tr '-' '_' \
     | cut -c 14- \
     | sed -r 's/(_)([a-z])/\U\2/g' \
     | sed 's/$/:"",/'
```

generate configuration CamelCase from helper for runner key word.

```shell
docker run gitlab/gitlab-runner:latest register --help \
     | grep RUNNER \
     | awk '{print $1}' \
     | sed 's/--//' \
     | tr '-' '_' \
     | sed -r 's/(_)([a-z])/\U\2/g' \
     | sed 's/$/:"",/'
```

generate configuration CamelCase from helper for CI key word.

```shell
docker run gitlab/gitlab-runner:latest register --help \
     | grep CI_ \
     | awk '{print $1}' \
     | sed 's/--//' \
     | tr '-' '_' \
     | sed -r 's/(_)([a-z])/\U\2/g' \
     | sed 's/$/:"",/'
```

generate configuration CamelCase from helper for register key word.

```shell
docker run gitlab/gitlab-runner:latest register --help \
     | grep REGISTER_ \
     | awk '{print $1}' \
     | sed 's/--//' \
     | tr '-' '_' \
     | sed -r 's/(_)([a-z])/\U\2/g' \
     | sed 's/$/:"",/'
```

generate configuration CamelCase from helper for custom key word.

```shell
docker run gitlab/gitlab-runner:latest register --help \
     | grep CUSTOM_ \
     | awk '{print $1}' \
     | sed 's/--//' \
     | tr '-' '_' \
     | sed -r 's/(_)([a-z])/\U\2/g' \
     | sed 's/$/:"",/'
```

generate configuration CamelCase from helper for custom key word.

```shell
docker run gitlab/gitlab-runner:latest register --help \
     | grep CACHE_ \
     | awk '{print $1}' \
     | sed 's/--//' \
     | tr '-' '_' \
     | sed -r 's/(_)([a-z])/\U\2/g' \
     | sed 's/$/:"",/'
```

# runner configuration

https://docs.gitlab.com/runner/configuration/advanced-configuration.html

# runner kubernetes configuration

https://docs.gitlab.com/runner/executors/kubernetes.html
https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerskubernetes-section

# shm-size for kubernetes

https://medium.com/dive-into-ml-ai/raising-shared-memory-limit-of-a-kubernetes-container-62aae0468a33

# pull policy security always consideration for shared runner

https://docs.gitlab.com/runner/security/index.html#usage-of-private-docker-images-with-if-not-present-pull-policy

# container lifecycle 

integration with ECA?

https://docs.gitlab.com/runner/executors/kubernetes.html#container-lifecycle-hooks