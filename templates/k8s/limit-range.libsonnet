// SPDX-FileCopyrightText: 2024 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

local Kube = import 'kube.libsonnet';
local Const = std.extVar('packs');

{
  gen(config): Kube.LimitRange(config.project.kubeNamespaceName, config) {
    spec: {

      local ressources = config.deployment.resources,
      local spec = self,
      
      local hardctx = {
        [if ctxValue.resourceCalculation then 'hard'+ctxValue.ctx]: {          
          runnerPodCpuRequest: ctxValue.resources.runner.cpuRequest,
          runnerPodMemoryRequest: ctxValue.resources.runner.memoryRequest,          

          runnerPodCpuLimit: ctxValue.resources.runner.cpuLimit,
          runnerPodMemoryLimit: ctxValue.resources.runner.memoryLimit,          

          buildPodCpuRequest: ctxValue.resources.init.cpuRequest + ctxValue.resources.build.cpuRequest + (if ctxValue.servicesActive then ctxValue.resources.service.cpuRequest else 0) + ctxValue.resources.helper.cpuRequest,
          buildPodMemoryRequest: ctxValue.resources.init.memoryRequest + ctxValue.resources.build.memoryRequest + (if ctxValue.servicesActive then ctxValue.resources.service.memoryRequest else 0) + ctxValue.resources.helper.memoryRequest,

          buildPodCpuLimit: ctxValue.resources.init.cpuLimit + ctxValue.resources.build.cpuLimit + (if ctxValue.servicesActive then ctxValue.resources.service.cpuLimit else 0) + ctxValue.resources.helper.cpuLimit,
          buildPodMemoryLimit: ctxValue.resources.init.memoryLimit + ctxValue.resources.build.memoryLimit + (if ctxValue.servicesActive then ctxValue.resources.service.memoryLimit else 0) + ctxValue.resources.helper.memoryLimit,

          runnerContainerCpuLimit: ctxValue.resources.runner.cpuLimit,
          runnerContainerMemoryLimit: ctxValue.resources.runner.memoryLimit,
          buildContainerCpuLimit: ctxValue.resources.build.cpuLimit,
          buildContainerMemoryLimit: ctxValue.resources.build.memoryLimit,
          helperContainerCpuLimit: ctxValue.resources.helper.cpuLimit,
          helperContainerMemoryLimit: ctxValue.resources.helper.memoryLimit,
          initContainerCpuLimit: ctxValue.resources.init.cpuLimit,
          initContainerMemoryLimit: ctxValue.resources.init.memoryLimit,
          serviceContainerCpuLimit: if ctxValue.servicesActive then ctxValue.resources.service.cpuLimit else 0,
          serviceContainerMemoryLimit: if ctxValue.servicesActive then ctxValue.resources.service.memoryLimit else 0,


          runnerContainerCpuRequest: ctxValue.resources.runner.cpuRequest,
          runnerContainerMemoryRequest: ctxValue.resources.runner.memoryRequest,
          buildContainerCpuRequest: ctxValue.resources.build.cpuRequest,
          buildContainerMemoryRequest: ctxValue.resources.build.memoryRequest,
          helperContainerCpuRequest: ctxValue.resources.helper.cpuRequest,
          helperContainerMemoryRequest: ctxValue.resources.helper.memoryRequest,
          initContainerCpuRequest: ctxValue.resources.init.cpuRequest,
          initContainerMemoryRequest: ctxValue.resources.init.memoryRequest,
          serviceContainerCpuRequest: if ctxValue.servicesActive then ctxValue.resources.service.cpuRequest else 0,
          serviceContainerMemoryRequest: if ctxValue.servicesActive then ctxValue.resources.service.memoryRequest else 0,
        },
        for ctxValue in std.objectValues(config.deploymentctx)
      },
      
      local calculateMaxLimit(propName) = 
         std.foldl(function(x, y) Kube.max2(x, y), std.map(function(x) x[propName], std.objectValues(hardctx)), 0),


      local calculateMinRequest(propName) = 
         std.foldl(function(x, y) Kube.min2(x, y), std.map(function(x) x[propName], std.objectValues(hardctx)), 1000),
      
      //debug: hardctx, 
      
      // Pod   
      local runnerPodCpuLimit = calculateMaxLimit('runnerPodCpuLimit'),
      local runnerPodMemoryLimit = calculateMaxLimit('runnerPodMemoryLimit'),
      local buildPodCpuLimit = calculateMaxLimit('buildPodCpuLimit'),
      local buildPodMemoryLimit = calculateMaxLimit('buildPodMemoryLimit'),
      
      local runnerPodCpuRequest = calculateMinRequest('runnerPodCpuRequest'),
      local runnerPodMemoryRequest = calculateMinRequest('runnerPodMemoryRequest'),
      local buildPodCpuRequest = calculateMinRequest('buildPodCpuRequest'),
      local buildPodMemoryRequest = calculateMaxLimit('buildPodMemoryRequest'),

      // Container

      local runnerContainerCpuLimit = calculateMaxLimit('runnerContainerCpuLimit'),
      local runnerContainerMemoryLimit = calculateMaxLimit('runnerContainerMemoryLimit'),
      local buildContainerCpuLimit = calculateMaxLimit('buildContainerCpuLimit'),
      local buildContainerMemoryLimit = calculateMaxLimit('buildContainerMemoryLimit'),
      local helperContainerCpuLimit = calculateMaxLimit('helperContainerCpuLimit'),
      local helperContainerMemoryLimit = calculateMaxLimit('helperContainerMemoryLimit'),
      local initContainerCpuLimit = calculateMaxLimit('initContainerCpuLimit'),
      local initContainerMemoryLimit = calculateMaxLimit('initContainerMemoryLimit'),
      local serviceContainerCpuLimit = calculateMaxLimit('serviceContainerCpuLimit'),
      local serviceContainerMemoryLimit = calculateMaxLimit('serviceContainerMemoryLimit'),


      local runnerContainerCpuRequest = calculateMinRequest('runnerContainerCpuRequest'),
      local runnerContainerMemoryRequest = calculateMinRequest('runnerContainerMemoryRequest'),
      local buildContainerCpuRequest = calculateMinRequest('buildContainerCpuRequest'),
      local buildContainerMemoryRequest = calculateMinRequest('buildContainerMemoryRequest'),
      local helperContainerCpuRequest = calculateMinRequest('helperContainerCpuRequest'),
      local helperContainerMemoryRequest = calculateMinRequest('helperContainerMemoryRequest'),
      local initContainerCpuRequest = calculateMinRequest('initContainerCpuRequest'),
      local initContainerMemoryRequest = calculateMinRequest('initContainerMemoryRequest'),
      local serviceContainerCpuRequest = calculateMinRequest('serviceContainerCpuRequest'),
      local serviceContainerMemoryRequest = calculateMinRequest('serviceContainerMemoryRequest'),




      limits: [
        { 
          type: 'Pod',
          min: {
            cpu: '%dm' % Kube.min2(runnerPodCpuRequest, buildPodCpuRequest),
            memory: '%dMi' % Kube.min2(runnerPodMemoryRequest, buildPodMemoryRequest),
          },
          max: {
            cpu: '%dm' % Kube.max2(runnerPodCpuLimit, buildPodCpuLimit),
            memory: '%dMi' % Kube.max2(runnerPodMemoryLimit, buildPodMemoryLimit),
          },
        }, {
          type: 'Container',
          local minCpu = Kube.min4(runnerContainerCpuRequest, buildContainerCpuRequest, helperContainerCpuRequest, initContainerCpuRequest),
          local minMemory = Kube.min4(runnerContainerMemoryRequest, buildContainerMemoryRequest, helperContainerMemoryRequest, initContainerMemoryRequest),
          min: {
            cpu: '%dm' % minCpu,
            memory: '%dMi' % minMemory,
          },
          default: {
            cpu: '%dm' % minCpu,
            memory: '%dMi' % minMemory,
          },
          defaultRequest: {
            cpu: '%dm' % minCpu,
            memory: '%dMi' % minMemory,
          },
          max: {
            cpu: '%dm' % Kube.max5(runnerContainerCpuLimit, buildContainerCpuLimit, helperContainerCpuLimit, initContainerCpuLimit, serviceContainerCpuLimit),
            memory: '%dMi' % Kube.max5(runnerContainerMemoryLimit, buildContainerMemoryLimit, helperContainerMemoryLimit, initContainerMemoryLimit, serviceContainerMemoryLimit),
          },
        },
      ],
    },
  },
}
