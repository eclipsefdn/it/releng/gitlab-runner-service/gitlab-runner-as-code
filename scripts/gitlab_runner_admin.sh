#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# GitLab admin functions

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${0}")")"

# shellcheck disable=SC1091
source "${SCRIPT_FOLDER}/gitlab_context.sh"

help() {
  printf "Available commands:\n"
  printf "Command\t\t\tDescription\n\n"
  printf "get_runner_token\t\tgenerate a new runner and return token.\n"
  exit 0
}

#######################################
# Get gitlab runner Token from project or group  
# ARGUMENTS:
#   repo_name: project or group as describe with path_with_namespace in projects api or full_path in groups api 
#   type: projects or groups
# RETURN:
#   token or null if not find
#######################################
get_runner_token() {
  local repo_name="${1:-}"
  local runner_name="${2:-}"
  local runner_description="${3:-}"
  local runner_locked="${4:-}"
  local runner_untagged="${5:-}"
  local runner_taglist="${6:-}"
  local type="${7:-}"

  local http_code
  local content
  check_parameter "repo name" "${repo_name}"

  local repo_name_encode
  repo_name_encode="$(urlencode "${repo_name}")"
 
  [[ -z "$type" ]] && type=$("${SCRIPT_FOLDER}/gitlab_groups_projects.sh" get_repo_type "${repo_name}") 

  local repo_id
  repo_id=$("${SCRIPT_FOLDER}/gitlab_groups_projects.sh" get_repo_id "${repo_name}" "${type}") 

  local runner_type
  if [[ "${type}" == "projects" ]]; then
    project_group_id=(--data "project_id=${repo_id}")
    runner_type="project_type"
  else
    project_group_id=(--data "group_id=${repo_id}")
    runner_type="group_type"
  fi

  runner_description_data=""
  if [[ -n "${runner_description}" ]]; then
    runner_description_data=(--data "description=${runner_description}")
  fi
  runner_locked_data=""
  if [[ -n "${runner_locked}" ]]; then
    runner_locked_data=(--data "locked=${runner_locked}")
  fi
  runner_untagged_data=""
  if [[ -n "${runner_untagged}" ]]; then
    runner_untagged_data=(--data "run_untagged=${runner_untagged}")
  fi
  runner_taglist_data=""
  if [[ -n "${runner_taglist}" ]]; then
    runner_taglist_data=(--data "tag_list=${runner_taglist}")
  fi
  # shellcheck disable=SC2068
  response=$(curl -sSL -w "\n%{http_code}" --request POST --header "${TOKEN_HEADER}" "${API_BASE_URL}/user/runners" \
    --data "runner_type=${runner_type}" \
    --data "name=${runner_name}" \
    ${runner_description_data[@]} \
    ${runner_locked_data[@]} \
    ${runner_untagged_data[@]} \
    ${runner_taglist_data[@]} \
    ${project_group_id[@]} \
  )

  http_code=$(echo "$response" | tail -n 1)

  # Extract content from the response variable (excluding the last line with the HTTP code)
  content=$(echo "$response" | head -n -1)

  # Check the HTTP response code and handle accordingly
  if [[ "$http_code" -eq 200 ]] || [[ "$http_code" -eq 201 ]]; then
    echo "$content"| jq .token | tr -d '"'
  else
    log error "Request failed for ${repo_name}. HTTP code: $http_code"
    exit 1
  fi
}

#######################################
# Get gitlab runner list from project or group  
# ARGUMENTS:
#   repo_name: project or group as describe with path_with_namespace in projects api or full_path in groups api 
#   type: projects or groups
#   tag_list: find by tag
# RETURN:
#   list of gitlab runner 
#######################################
get_runner_list() {
  local repo_name="${1:-}"
  local type="${2:-}"
  local tag_list="${3:-}"

  check_parameter "repo name" "${repo_name}"

  local repo_name_encode
  repo_name_encode="$(urlencode "${repo_name}")"

  [[ -z "$type" ]] && type=$("${SCRIPT_FOLDER}/gitlab_groups_projects.sh" get_repo_type "${repo_name}") 

  local tag_search=""
  [[ -n "${tag_list}" ]] && tag_search="tag_list=${tag_list}"
  
  response=$(curl -sSL --header "${TOKEN_HEADER}" -w "\n%{http_code}" "${API_BASE_URL}/${type}/${repo_name_encode}/runners?${tag_search}")

  http_code=$(echo "$response" | tail -n 1)

  # Extract content from the response variable (excluding the last line with the HTTP code)
  content=$(echo "$response" | head -n -1)

  # Check the HTTP response code and handle accordingly
  if [ "$http_code" -eq 200 ]; then
      echo "$content"
  else
    log error "Request failed. HTTP code: $http_code"
    exit 1
  fi
}

#######################################
# Get gitlab runner id list from project or group  
# ARGUMENTS:
#   repo_name: project or group as describe with path_with_namespace in projects api or full_path in groups api 
#   type: projects or groups
#   tag_list: find by tag
# RETURN:
#   list of gitlab runner id or null
#######################################
get_runner_list_id() {
  local repo_name="${1:-}"
  local type="${2:-}"
  local tag_list="${3:-}"

  runner_list="$(get_runner_list "${repo_name}" "${type}"  "${tag_list}")"
  echo "$runner_list" | jq -r '.[].id'
}


get_project_runner_list() {
  local repo_name="${1:-}"
  local tag_list="${2:-}"

  check_parameter "repo name" "${repo_name}"

  local runner_list
  runner_list="$(get_runner_list_id "${repo_name}" "projects" "${tag_list}")"
  echo "$runner_list"
}

get_group_runner_list() {
  local repo_name="${1:-}"
  local tag_list="${2:-}"

  check_parameter "repo name" "${repo_name}"

  local runner_list
  runner_list="$(get_runner_list_id "${repo_name}" "groups"  "${tag_list}")"
  echo "$runner_list"
}

#######################################
# Delete gitlab runner from project or group  
# ARGUMENTS:
#   runner_id: id of the runner
#######################################
delete_runner_by_id() {
  local runner_id="${1:-}"

  check_parameter "runner id" "${runner_id}"

  log info "Delete runner ${runner_id}"
  curl -sSL --request DELETE --header "${TOKEN_HEADER}" "${API_BASE_URL}/runners/${runner_id}"
}

delete_project_runner() {
  local repo_name="${1:-}"
  local tag_list="${2:-}"

  check_parameter "repo name" "${repo_name}"
  
  log info "Start delete runners from project '${GITLAB_URL}/${repo_name}'"

  local runner_list
  runner_list="$(get_project_runner_list "${repo_name}" "${tag_list}")"

  log info  "Delete runners from project '${GITLAB_URL}/${repo_name}' with tags list: '${tag_list}'"
  for runner_id in ${runner_list}
  do  
    delete_runner_by_id "${runner_id}"
  done
}

delete_group_runner() {
  local repo_name="${1:-}"
  local tag_list="${2:-}"

  check_parameter "repo name" "${repo_name}"

  log info "Start delete runners from group '${GITLAB_URL}/$repo_name'"

  local runner_list
  runner_list="$(get_group_runner_list "${repo_name}" "${tag_list}")"

  log info "Delete runners from group '${GITLAB_URL}/$repo_name' with tags list: '${tag_list}'"
  for runner_id in ${runner_list}
  do
    delete_runner_by_id "${runner_id}"
  done
}

delete_runner() {
  local repo_name="${1:-}"
  local type="${2:-}"
  local tag_list="${3:-}"

  check_parameter "repo name" "${repo_name}"

  log info "Start Delete runners from repo '${GITLAB_URL}/${repo_name}'"

  [[ -z "$type" ]] && type=$("${SCRIPT_FOLDER}/gitlab_groups_projects.sh" get_repo_type "${repo_name}") 

  local runner_list
  runner_list="$(get_runner_list_id "${repo_name}" "" "${tag_list}")"

  if [[ -z ${runner_list} ]]; then
    log info "No runners find to delete from repo '${GITLAB_URL}/${repo_name}' with tags list: '${tag_list}'"
  else
    log info "Delete runners from repo '${GITLAB_URL}/${repo_name}' with tags list: '${tag_list}'"
  fi 
  for runner_id in ${runner_list}
  do
    delete_runner_by_id "${runner_id}"
  done
}

set_CI_variables() {
  local repo_name="${1:-}"
  local type="${2:-}"
  local key="${3:-}"
  local value="${4:-}"

  check_parameter "repo name" "${repo_name}"
  check_parameter "key" "${key}"
  check_parameter "value" "${value}"

  local repo_name_encode
  repo_name_encode="$(urlencode "${repo_name}")"
  [[ -z "$type" ]] && type=$("${SCRIPT_FOLDER}/gitlab_groups_projects.sh" get_repo_type "${repo_name}") 

  curl -sSL --request POST --header "${TOKEN_HEADER}" "${API_BASE_URL}/${type}/${repo_name_encode}/variables" --form "key=${key}" --form "value=${value}"
}

update_CI_variables() {
  local repo_name="${1:-}"
  local type="${2:-}"
  local key="${3:-}"
  local value="${4:-}"

  check_parameter "repo name" "${repo_name}"
  check_parameter "key" "${key}"
  check_parameter "value" "${value}"

  local repo_name_encode
  repo_name_encode="$(urlencode "${repo_name}")"
  [[ -z "$type" ]] && type=$("${SCRIPT_FOLDER}/gitlab_groups_projects.sh" get_repo_type "${repo_name}") 

  curl -sSL --request DELETE --header "${TOKEN_HEADER}" "${API_BASE_URL}/${type}/${repo_name_encode}/variables/${key}"
  set_CI_variables "${repo_name}" "${type}" "${key}" "${value}"
}


"$@"

# show help menu, if no first parameter is given
if [[ -z "${1:-}" ]]; then
  help
fi