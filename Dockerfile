
# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

FROM node:21-bookworm AS build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN npm install typescript@"^5.3.2" -g

# hadolint ignore=DL3016
#RUN npm install -g hbs-cli
WORKDIR /opt
# RUN git clone https://github.com/keithamus/hbs-cli.git
RUN git clone https://github.com/heurtematte/hbs-cli.git 
WORKDIR /opt/hbs-cli
RUN node --version && npm ci

FROM buildpack-deps:bookworm

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG DEBIAN_FRONTEND="noninteractive"
ARG OCP_VERSION=4.13.0
ARG YQ_VERSION=latest

# DL3008 Pin versions in apt get install
# hadolint ignore=DL3008
RUN apt-get update -qq && \
    apt-get install -y eatmydata --no-install-recommends && \
    eatmydata apt-get install -y -qq --no-install-recommends \
        bc \
        jq \
        make \
        jsonnet \
        nodejs \
        wget \
        parallel \
        curl && \
    apt-get upgrade -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives

COPY --from=build /opt/hbs-cli /opt/hbs-cli
RUN ln -s /opt/hbs-cli/lib/src/index.js /usr/local/bin/hbs  && chmod +x /usr/local/bin/hbs

ADD https://github.com/mikefarah/yq/releases/${YQ_VERSION}/download/yq_linux_amd64 /usr/local/bin/yq 
RUN chmod a+x /usr/local/bin/yq

RUN wget --progress=dot:giga -O - "https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/${OCP_VERSION}/openshift-client-linux.tar.gz" | tar xz  -C /usr/local/bin && \
    chmod +x /usr/local/bin/{kubectl,oc}

# RUN adduser --disabled-password --gecos '' grac

RUN groupadd --gid 1000 grac \
  && useradd --uid 1000 --gid grac --shell /bin/bash --create-home grac

WORKDIR /home/grac
COPY . /home/grac

USER grac

