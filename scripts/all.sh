#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2024 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

: "${GRAC_DEBUG:=""}"
[[ -n "${GRAC_DEBUG}" ]] && [[ "${GRAC_DEBUG}" == "true" ]] && set -x

IFS=$'\n\t'
SCRIPT_FOLDER="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"


# shellcheck disable=SC1091
 . "${SCRIPT_FOLDER}/log.sh"


action="${1:-}"

log info  "############################ START ACTION ALL ${action} ############################"

# shellcheck disable=SC1091
. "${SCRIPT_FOLDER}/k8s-context.sh" 

instances_dir="${2:-}"

if [ -z "${action}" ]; then
  log error "You must provide an 'action' argument"
  exit 1
fi

if [ -z "${instances_dir}" ]; then
  log error "You must provide an 'instance directory' argument"
  exit 1
fi

for instance in "${instances_dir}"/*
do
  if [ -d "$instance" ]; then
    start=$(date +"%s")
    log info "Start: ${instance}..."  
    [[ -f "${SCRIPT_FOLDER}/${action}.sh" ]] && \
      "${SCRIPT_FOLDER}/${action}.sh" "${instance}"

    end=$(date +"%s")
    runtime=$(echo "$end - $start" | bc -l)
    log info "End: ${instance} in ${runtime}s\n"  
  fi
done

log info  "############################ END ACTION ALL ${action}  ############################"